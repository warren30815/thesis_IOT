//
//  HumanActivityManager.swift
//  MetaWearApp
//
//  Created by 許竣翔 on 2021/5/29.
//

import MBProgressHUD

class HumanActivityManager {
    var phone_acc_mag_data :[Float] = [Float]()
    var wristband_acc_mag_data :[Float] = [Float]()
    var stream_wristband_acc_data : [[Float]] = [[Float]]()
    var stream_wristband_q_data : [[Float]] = [[Float]]()
    var timer: Timer!
    var current_check_start_index = 0
    var motion_index_set = Set<Int>()
    var gesture_label_list : [String] = [String]()
    let sequence_length = 30
    let step = 2
    let lag = 15
    let threshold = 5
    let influence = 0.8
    var peakManager : PeakDetection
    
    init()
    {
        peakManager = PeakDetection(y: Array(repeating: 0.0, count: lag), lag: lag, threshold: Double(threshold), influence: influence)
        phone_acc_mag_data.removeAll()
        wristband_acc_mag_data.removeAll()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.02 * Double(step),
                target: self, selector: #selector(self.checkIsInMotion),
                userInfo: nil, repeats: true )
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    func labelGesture(gesture: String) {
        let remainder = current_check_start_index % 5
        var insertIndex : Int
        if remainder < 3 {
            insertIndex = current_check_start_index - remainder
        } else {
            insertIndex = current_check_start_index + (5 - remainder)
        }
        let gesture_and_time = gesture + "-" + String(insertIndex)
        let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
        hud.label.text = gesture_and_time
        hud.hide(animated: true, afterDelay: 0.2)
        hud.mode = .text
        gesture_label_list.append(gesture_and_time)
    }
    
    func phoneNext(_ val:Float)
    {
        phone_acc_mag_data.append(val)
    }
    
    func wristbandNext(_ val:Float)
    {
        wristband_acc_mag_data.append(val)
    }
    
    func wristbandStreamNext(type: String, val: [Float])
    {
        if type == "a"{
            stream_wristband_acc_data.append(val)
        } else if type == "q"{
            stream_wristband_q_data.append(val)
        }
    }
    
    func getPhoneDataCount() -> Int{
        return phone_acc_mag_data.count
    }
    
    func getWristDataCount() -> Int{
        return wristband_acc_mag_data.count
    }

    func compute_avg_difference(array1: [Float], array2: [Float]) -> Float {
        var sum: Float = 0.0

        for i in 0 ..< array1.count {
            let temp =  Float(abs(array1[i] - array2[i]))
            sum = sum + temp
        }
        let avg = sum / Float(array1.count)
        return avg
    }
    
    func compute_difference(array1: [Float], array2: [Float]) -> [Double] {
        var diff_array = [Double]()
        for i in 0 ..< array1.count {
            let temp =  Double(array1[i] - array2[i])
            diff_array.append(temp)
        }
        return diff_array
    }
    
    func sliceArray(numbers: [Float], start_position: Int) -> Array<Float> {
        let newArray = Array(numbers[start_position ..< start_position + sequence_length])
        return newArray
    }
    
    func sliceArray(numbers: [[Float]], start_position: Int, end_position: Int) -> [[Float]] {
        let newArray = Array(numbers[start_position ..< end_position])
        return newArray
    }
    
    // Function to extract some range from an array
    func subArray<T>(array: [T], s: Int, e: Int) -> [T] {
        if e > array.count {
            return []
        }
        return Array(array[s..<min(e, array.count)])
    }
    
    func get_all_data() -> Data {
        var signal_data = Data()
        let end_index = min(stream_wristband_q_data.count, stream_wristband_acc_data.count)
        for i in 0 ... end_index - 1 {
            signal_data.append("q,\(stream_wristband_q_data[i][0]),\(stream_wristband_q_data[i][1]),\(stream_wristband_q_data[i][2]),\(stream_wristband_q_data[i][3])\n".data(using: String.Encoding.utf8)!)
            signal_data.append("a,\(stream_wristband_acc_data[i][0]),\(stream_wristband_acc_data[i][1]),\(stream_wristband_acc_data[i][2]),\(stream_wristband_acc_data[i][3])\n".data(using: String.Encoding.utf8)!)
        }
//        for i in 0 ... phone_acc_mag_data.count - 1 {
//            signal_data.append("phoneA,\(phone_acc_mag_data[i])\n".data(using: String.Encoding.utf8)!)
//        }
        if (!motion_index_set.isEmpty) {
            let motion_index_list = motion_index_set.sorted()
            var result_set = Set<Int>()
            var result_list : [Int] = [Int]()
            print("Final: ")
            for i in 0 ... motion_index_list.count - 1 {
                var flag = false
                if (i > 0 && i != motion_index_list.count - 1) {
                    let curr = motion_index_list[i]
                    let prev = motion_index_list[i - 1]
                    let post = motion_index_list[i + 1]
                    if (curr == prev + 5 || curr == post - 5) {
                        flag = true
                    } else {
                        flag = false
                    }
                } else if (i == motion_index_list.count - 1) {
                    let curr = motion_index_list[i]
                    let prev = motion_index_list[i - 1]
                    if (curr == prev + 5) {
                        flag = true
                    } else {
                        flag = false
                    }
                }
                if (flag == true) {
                    let middlePoint = motion_index_list[i]
                    let start = max(0, middlePoint-150)
                    let end = min(current_check_start_index, middlePoint+150)
                    for index in stride(from: start, to: end, by: 5) {
                        result_set.insert(index)
                    }
                }
            }
            result_list = result_set.sorted()
            for i in 0 ... result_list.count - 1 {
                signal_data.append("m,\(i)\n".data(using: String.Encoding.utf8)!)
            }
            print(result_list)
        }
        if (!gesture_label_list.isEmpty) {
            for i in 0 ... gesture_label_list.count - 1 {
                signal_data.append("l,\(gesture_label_list[i])\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        reset()
        return signal_data
    }
    
    var peakManagerInitDone = false
    @objc func checkIsInMotion() {
        if (!peakManagerInitDone && phone_acc_mag_data.count >= sequence_length && wristband_acc_mag_data.count >= sequence_length) {
            let samples = compute_difference(array1: phone_acc_mag_data, array2: wristband_acc_mag_data)
            let y = subArray(array: samples, s: sequence_length - lag, e: sequence_length)
            peakManager = PeakDetection(y: y, lag: lag, threshold: Double(threshold), influence: influence)
            peakManagerInitDone = true
        }
        
        if (peakManagerInitDone) {
            if phone_acc_mag_data.count < current_check_start_index + sequence_length || wristband_acc_mag_data.count < current_check_start_index + sequence_length {
                return
            } else {
                let diff = abs(phone_acc_mag_data[current_check_start_index] - wristband_acc_mag_data[current_check_start_index])
                let peak = peakManager.detectNewValue(new_value: Double(diff))
                if peak == 1 {
                    let remainder = current_check_start_index % 5
                    var insertIndex : Int
                    if remainder < 3 {
                        insertIndex = current_check_start_index - remainder
                    } else {
                        insertIndex = current_check_start_index + (5 - remainder)
                    }
                    motion_index_set.insert(insertIndex)
                    print(motion_index_set.sorted())
                }
                current_check_start_index += step
            }
        }
    }
    
    func reset() {
        stopTimer()
        phone_acc_mag_data.removeAll()
        wristband_acc_mag_data.removeAll()
        stream_wristband_acc_data.removeAll()
        stream_wristband_q_data.removeAll()
        motion_index_set.removeAll()
        gesture_label_list.removeAll()
        current_check_start_index = 0
        peakManagerInitDone = false
    }
}

