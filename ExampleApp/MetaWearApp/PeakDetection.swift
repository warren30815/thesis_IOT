//
//  HumanActivityManager.swift
//  MetaWearApp
//
//  Created by 許竣翔 on 2021/5/29.
//

// ref: https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/68592149#68592149
class PeakDetection {
//    var phone_acc_mag_data :[Float] = [Float]()
//    var wristband_acc_mag_data :[Float] = [Float]()
//    var stream_wristband_acc_data : [[Float]] = [[Float]]()
//    var stream_wristband_q_data : [[Float]] = [[Float]]()
    var y : [Double]
    var signals : Array<Int>
    var filteredY : Array<Double>
    var avgFilter : Array<Double>
    var stdFilter : Array<Double>
    var lag : Int
    var threshold : Double
    var influence : Double
    
    init(y: [Double],lag: Int,threshold: Double,influence: Double)
    {
        // Initialise variables
        self.y = y
        self.signals   = Array(repeating: 0, count: y.count)
        self.filteredY = y
        self.avgFilter = Array(repeating: 0.0, count: y.count)
        self.stdFilter = Array(repeating: 0.0, count: y.count)
        self.lag = lag
        self.threshold = threshold
        self.influence = influence
        
        // Init filter
        avgFilter[lag-1] = arithmeticMean(array: subArray(array: y, s: 0, e: lag-1))
        stdFilter[lag-1] = standardDeviation(array: subArray(array: y, s: 0, e: lag-1))
        print(avgFilter)
    }
    
    // Smooth z-score thresholding filter
    func detectNewValue(new_value: Double) -> Int {
        y.append(new_value)
        let i = self.y.count - 1
        
        signals.append(0)
        filteredY.append(0)
        avgFilter.append(0)
        stdFilter.append(0)

        if (abs(y[i] - avgFilter[i - 1]) > threshold * stdFilter[i - 1]) {
            if y[i] > avgFilter[i - 1] {
                signals[i] = 1
            } else {
                // Negative signals are turned off for this application
                //signals[i] = -1       // Negative signal
            }

            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i - 1]
        } else {
            signals[i] = 0
            filteredY[i] = y[i]
        }
        avgFilter[i] = arithmeticMean(array: subArray(array: filteredY, s: i-lag, e: i))
        stdFilter[i] = standardDeviation(array: subArray(array: filteredY, s: i-lag, e: i))
        return signals[i]
    }
    
    // Function to calculate the arithmetic mean
    func arithmeticMean(array: [Double]) -> Double {
        var total: Double = 0
        for number in array {
            total += number
        }
        return total / Double(array.count)
    }

    // Function to calculate the standard deviation
    func standardDeviation(array: [Double]) -> Double
    {
        let length = Double(array.count)
        let avg = array.reduce(0, {$0 + $1}) / length
        let sumOfSquaredAvgDiff = array.map { pow($0 - avg, 2.0)}.reduce(0, {$0 + $1})
        return sqrt(sumOfSquaredAvgDiff / length)
    }

    // Function to extract some range from an array
    func subArray<T>(array: [T], s: Int, e: Int) -> [T] {
        if e > array.count {
            return []
        }
        return Array(array[s..<min(e, array.count)])
    }
    
    func reset() {
    }
}


