//
//  DeviceDetailViewController.swift
//  MetaWearApiTest
//
//  Created by Stephen Schiffli on 11/3/16.
//  Copyright © 2016 MbientLab. All rights reserved.
//

import UIKit
import StaticDataTableViewController
import MetaWear
import MetaWearCpp
import MessageUI
import BoltsSwift
import MBProgressHUD
import iOSDFULibrary
import SceneKit
import Plot3d
import Accelerate
import CoreMotion
import Alamofire

extension String {
    var drop0xPrefix: String {
        return hasPrefix("0x") ? String(dropFirst(2)) : self
    }
}

class DeviceDetailViewController: StaticDataTableViewController {
    // delegate: PlotDataSource, PlotDelegate
    var device: MetaWear!
    var bmi270: Bool = false
    
    @IBOutlet weak var connectionSwitch: UISwitch!
    @IBOutlet weak var connectionStateLabel: UILabel!
    
    @IBOutlet var allCells: [UITableViewCell]!
    
    @IBOutlet var infoAndStateCells: [UITableViewCell]!
    @IBOutlet weak var serialNumLabel: UILabel!
    @IBOutlet weak var modelNumberLabel: UILabel!
    @IBOutlet weak var batteryLevelLabel: UILabel!
    @IBOutlet weak var rssiLevelLabel: UILabel!

    
    @IBOutlet weak var accelerometerBMI160Cell: UITableViewCell!
    @IBOutlet weak var accelerometerBMI160Scale: UISegmentedControl!
    @IBOutlet weak var accelerometerBMI160Frequency: UISegmentedControl!
    @IBOutlet weak var accelerometerBMI160StartStream: UIButton!
    @IBOutlet weak var accelerometerBMI160StopStream: UIButton!
    @IBOutlet weak var accelerometerBMI160StartLog: UIButton!
    @IBOutlet weak var accelerometerBMI160StopLog: UIButton!
    @IBOutlet weak var accelerometerBMI160Graph: APLGraphView!
    @IBOutlet weak var accelerometerBMI160StartOrient: UIButton!
    @IBOutlet weak var accelerometerBMI160StopOrient: UIButton!
    @IBOutlet weak var accelerometerBMI160OrientLabel: UILabel!
    @IBOutlet weak var accelerometerBMI160StartStep: UIButton!
    @IBOutlet weak var accelerometerBMI160StopStep: UIButton!
    @IBOutlet weak var accelerometerBMI160StepLabel: UILabel!
    var accelerometerBMI160StepCount = 0
    var accelerometerBMI160Data: [(Int64, MblMwCartesianFloat)] = []
    
    @IBOutlet weak var gyroBMI160Cell: UITableViewCell!
    @IBOutlet weak var gyroBMI160Scale: UISegmentedControl!
    @IBOutlet weak var gyroBMI160Frequency: UISegmentedControl!
    @IBOutlet weak var gyroBMI160StartStream: UIButton!
    @IBOutlet weak var gyroBMI160StopStream: UIButton!
    @IBOutlet weak var gyroBMI160StartLog: UIButton!
    @IBOutlet weak var gyroBMI160StopLog: UIButton!
    @IBOutlet weak var gyroBMI160Graph: APLGraphView!
    var gyroBMI160Data: [(Int64, MblMwCartesianFloat)] = []
    
    @IBOutlet weak var sensorFusionCell: UITableViewCell!
    @IBOutlet weak var sensorFusionMode: UISegmentedControl!
    @IBOutlet weak var sensorFusionOutput: UISegmentedControl!
    @IBOutlet weak var sensorFusionStartStream: UIButton!
    @IBOutlet weak var sensorFusionStopStream: UIButton!
    @IBOutlet weak var sensorFusionStartLog: UIButton!
    @IBOutlet weak var sensorFusionStopLog: UIButton!
    @IBOutlet weak var chart: UIView!
    
    @IBOutlet weak var sensorFusionGraph: APLGraphView!
    
//    @IBOutlet weak var activityLabel: UITextView!
    
//    var plotView: PlotView!
    
    var sensorFusionData = Data()
    var gyroData = Data()
    
    var streamingEvents: Set<OpaquePointer> = []
    var streamingCleanup: [OpaquePointer: () -> Void] = [:]
    var loggers: [String: OpaquePointer] = [:]
    
    var disconnectTask: Task<MetaWear>?
    var isObserving = false {
        didSet {
            if self.isObserving {
                if !oldValue {
                    self.device.peripheral.addObserver(self, forKeyPath: "state", options: .new, context: nil)
                }
            } else {
                if oldValue {
                    self.device.peripheral.removeObserver(self, forKeyPath: "state")
                }
            }
        }
    }
    var hud: MBProgressHUD!
    
    var controller: UIDocumentInteractionController!
    var initiator: DFUServiceInitiator?
    var dfuController: DFUServiceController?
    
    let manager = CMMotionManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
//        initPlotView()
    }

    @IBAction func draw(_ sender: Any) {
//        chart.addSubview(plotView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Use this array to keep track of all streaming events, so turn them off
        // in case the user isn't so responsible
        streamingEvents = []
        cells(self.allCells, setHidden: true)
        reloadData(animated: false)
        // Write in the 2 fields we know at time zero
        connectionStateLabel.text! = nameForState()
        // Listen for state changes
        isObserving = true
        // Start off the connection flow
        connectDevice(true)
    }
    
    override func showHeader(forSection section: Int, vissibleRows: Int) -> Bool {
        return vissibleRows != 0
    }
    
    override func showFooter(forSection section: Int, vissibleRows: Int) -> Bool {
        return vissibleRows != 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        isObserving = false
        streamingCleanup.forEach { $0.value() }
        streamingCleanup.removeAll()
        mbl_mw_led_stop_and_clear(device.board)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        OperationQueue.main.addOperation {
            self.connectionStateLabel.text! = self.nameForState()
            if self.device.peripheral.state == .disconnected {
                self.deviceDisconnected()
            }
        }
    }
    
    func nameForState() -> String {
        switch device.peripheral.state {
        case .connected:
            return "Connected"
        case .connecting:
            return "Connecting"
        case .disconnected:
            return "Disconnected"
        case .disconnecting:
            return "Disconnecting"
        }
    }
    
    func logCleanup(_ handler: @escaping (Error?) -> Void) {
        // In order for the device to actaully erase the flash memory we can't be in a connection
        // so temporally disconnect to allow flash to erase.
        isObserving = false
        device.connectAndSetup().continueOnSuccessWithTask { t -> Task<MetaWear> in
            self.device.cancelConnection()
            return t
        }.continueOnSuccessWithTask { t -> Task<Task<MetaWear>> in
            return self.device.connectAndSetup()
        }.continueWith { t in
            self.isObserving = true
            handler(t.error)
        }
    }
    
    func showAlertTitle(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deviceDisconnected() {
        connectionSwitch.setOn(false, animated: true)
        cells(self.allCells, setHidden: true)
        reloadData(animated: true)
    }
    
    func deviceConnectedReadAnonymousLoggers() {
        let task = device.createAnonymousDatasignals()
        task.continueWith(.mainThread) { t in
            //print(self.loggers)
            if let signals = t.result {
                for signal in signals {
                    let cString = mbl_mw_anonymous_datasignal_get_identifier(signal)!
                    let identifier = String(cString: cString)
                    self.loggers[identifier] = signal
                }
            }
            self.deviceConnected()
        }
    }
    
    func deviceConnected() {
        connectionSwitch.setOn(true, animated: true)
        // Perform all device specific setup
        print("ID: \(self.device.peripheral.identifier.uuidString) MAC: \(self.device.mac ?? "N/A")")
        // We always have the info and state features
        cells(self.infoAndStateCells, setHidden: false)
        serialNumLabel.text = device.info?.serialNumber ?? "N/A"
        modelNumberLabel.text = "\(device.info?.modelNumber ?? "N/A") (\(String(cString: mbl_mw_metawearboard_get_model_name(device.board))))"
        // Automaticaly send off some reads
        mbl_mw_settings_get_battery_state_data_signal(device.board).read().continueOnSuccessWith(.mainThread) {
            let battery: MblMwBatteryState = $0.valueAs()
            self.batteryLevelLabel.text = String(battery.charge)
        }
        self.rssiLevelLabel.text = String(device.rssi)
        let board = device.board
        
        if mbl_mw_metawearboard_lookup_module(board, MBL_MW_MODULE_ACCELEROMETER) == MetaWearCpp.MBL_MW_MODULE_ACC_TYPE_BMI160 {
            cell(accelerometerBMI160Cell, setHidden: false)
            bmi270 = false
            if loggers["acceleration"] != nil {
                accelerometerBMI160StartLog.isEnabled = false
                accelerometerBMI160StopLog.isEnabled = true
                accelerometerBMI160StartStream.isEnabled = false
                accelerometerBMI160StopStream.isEnabled = false
            } else {
                accelerometerBMI160StartLog.isEnabled = true
                accelerometerBMI160StopLog.isEnabled = false
                accelerometerBMI160StartStream.isEnabled = true
                accelerometerBMI160StopStream.isEnabled = false
            }
        } else if mbl_mw_metawearboard_lookup_module(board, MBL_MW_MODULE_ACCELEROMETER) == MetaWearCpp.MBL_MW_MODULE_ACC_TYPE_BMI270 {
            cell(accelerometerBMI160Cell, setHidden: false)
            bmi270 = true
            if loggers["acceleration"] != nil {
                accelerometerBMI160StartLog.isEnabled = false
                accelerometerBMI160StopLog.isEnabled = true
                accelerometerBMI160StartStream.isEnabled = false
                accelerometerBMI160StopStream.isEnabled = false
            } else {
                accelerometerBMI160StartLog.isEnabled = true
                accelerometerBMI160StopLog.isEnabled = false
                accelerometerBMI160StartStream.isEnabled = true
                accelerometerBMI160StopStream.isEnabled = false
            }
        }
        
        if mbl_mw_metawearboard_lookup_module(board, MBL_MW_MODULE_GYRO) != MBL_MW_MODULE_TYPE_NA {
            cell(gyroBMI160Cell, setHidden: false)
            if loggers["angular-velocity"] != nil {
                gyroBMI160StartLog.isEnabled = false
                gyroBMI160StopLog.isEnabled = true
                gyroBMI160StartStream.isEnabled = false
                gyroBMI160StopStream.isEnabled = false
            } else {
                gyroBMI160StartLog.isEnabled = true
                gyroBMI160StopLog.isEnabled = false
                gyroBMI160StartStream.isEnabled = true
                gyroBMI160StopStream.isEnabled = false
            }
        }
        
        if mbl_mw_metawearboard_lookup_module(board, MBL_MW_MODULE_SENSOR_FUSION) != MBL_MW_MODULE_TYPE_NA {
            cell(sensorFusionCell, setHidden: false)
            var isLogging = true
            if loggers["euler-angles"] != nil {
                sensorFusionOutput.selectedSegmentIndex = 0
            } else if loggers["quaternion"] != nil {
                sensorFusionOutput.selectedSegmentIndex = 1
            } else if loggers["gravity"] != nil {
                sensorFusionOutput.selectedSegmentIndex = 2
            } else if loggers["linear-acceleration"] != nil {
                sensorFusionOutput.selectedSegmentIndex = 3
            } else {
                isLogging = false
            }
            
            if isLogging {
                sensorFusionStartLog.isEnabled = false
//                sensorFusionStopLog.isEnabled = true
                sensorFusionStopLog.isEnabled = false  // close temporarily
                sensorFusionStartStream.isEnabled = false
                sensorFusionStopStream.isEnabled = false
                sensorFusionMode.isEnabled = false
                sensorFusionOutput.isEnabled = false
            } else {
//                sensorFusionStartLog.isEnabled = true
                sensorFusionStartLog.isEnabled = false  // close temporarily
                sensorFusionStopLog.isEnabled = false
                sensorFusionStartStream.isEnabled = true
                sensorFusionStopStream.isEnabled = false
                sensorFusionMode.isEnabled = true
                sensorFusionOutput.isEnabled = true
            }
        }
        
        // Make the magic happen!
        reloadData(animated: true)
    }
    
    func connectDevice(_ on: Bool) {
        if on {
            let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
            hud.label.text = "Connecting..."
            device.connectAndSetup().continueWith(.mainThread) { t in
                hud.mode = .text
                if t.error != nil {
                    self.showAlertTitle("Error", message: t.error!.localizedDescription)
                    hud.hide(animated: false)
                } else {
                    self.deviceConnectedReadAnonymousLoggers()
                    hud.label.text! = "Connected!"
                    hud.hide(animated: true, afterDelay: 0.5)
                }
            }
        } else {
            device.cancelConnection()
        }
    }
    
    @IBAction func connectionSwitchPressed(_ sender: Any) {
        connectDevice(connectionSwitch.isOn)
    }
    
    @IBAction func readBatteryPressed(_ sender: Any) {
        mbl_mw_settings_get_battery_state_data_signal(device.board).read().continueWith(.mainThread) {
            if let error = $0.error {
                self.showAlertTitle("Error", message: error.localizedDescription)
            } else {
                let battery: MblMwBatteryState = $0.result!.valueAs()
                self.batteryLevelLabel.text = String(battery.charge)
            }
        }
    }
    
    @IBAction func readRSSIPressed(_ sender: Any) {
        device.readRSSI().continueOnSuccessWith(.mainThread) { rssi in
            self.rssiLevelLabel.text = String(rssi)
        }
    }
    
    @IBAction func resetDevicePressed(_ sender: Any) {
        // Resetting causes a disconnection
        deviceDisconnected()
        // Preform the soft reset
        mbl_mw_debug_reset(device.board)
    }
    
    @IBAction func factoryDefaultsPressed(_ sender: Any) {
        // Resetting causes a disconnection
        deviceDisconnected()
        // TODO: In case any pairing information is on the device mark it for removal too
        device.clearAndReset()
    }
    
    @IBAction func putToSleepPressed(_ sender: Any) {
        // Sleep causes a disconnection
        deviceDisconnected()
        // Set it to sleep after the next reset
        mbl_mw_debug_enable_power_save(device.board)
        // Preform the soft reset
        mbl_mw_debug_reset(device.board)
    }
    
    func send(_ data: Data, title: String, toWhere: String) {
        // Get current Time/Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM_dd_yyyy-HH_mm_ss"
        let dateString = dateFormatter.string(from: Date())
        let name = "\(title)_\(dateString).csv"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(name)
        if (toWhere == "custom") {
            do {
                try data.write(to: fileURL, options: .atomic)
                // Popup the default share screen
                self.controller = UIDocumentInteractionController(url: fileURL)
                if !self.controller.presentOptionsMenu(from: view.bounds, in: view, animated: true) {
                    self.showAlertTitle("Error", message: "No programs installed that could save the file")
                }
            } catch let error {
                self.showAlertTitle("Error", message: error.localizedDescription)
            }
        }
    }

    func updateAccelerometerBMI160Settings() {
        switch self.accelerometerBMI160Scale.selectedSegmentIndex {
        case 0:
            mbl_mw_acc_bosch_set_range(device.board, MBL_MW_ACC_BOSCH_RANGE_2G)
            self.accelerometerBMI160Graph.fullScale = 2
        case 1:
            mbl_mw_acc_bosch_set_range(device.board, MBL_MW_ACC_BOSCH_RANGE_4G)
            self.accelerometerBMI160Graph.fullScale = 4
        case 2:
            mbl_mw_acc_bosch_set_range(device.board, MBL_MW_ACC_BOSCH_RANGE_8G)
            self.accelerometerBMI160Graph.fullScale = 8
        case 3:
            mbl_mw_acc_bosch_set_range(device.board, MBL_MW_ACC_BOSCH_RANGE_16G)
            self.accelerometerBMI160Graph.fullScale = 16
        default:
            fatalError("Unexpected accelerometerBMI160Scale value")
        }
        mbl_mw_acc_set_odr(device.board, Float(accelerometerBMI160Frequency.titleForSegment(at: accelerometerBMI160Frequency.selectedSegmentIndex)!)!)
        mbl_mw_acc_bosch_write_acceleration_config(device.board)
    }

    @IBAction func accelerometerBMI160StartStreamPressed(_ sender: Any) {
        accelerometerBMI160StartStream.isEnabled = false
        accelerometerBMI160StopStream.isEnabled = true
        accelerometerBMI160StartLog.isEnabled = false
        accelerometerBMI160StopLog.isEnabled = false
        updateAccelerometerBMI160Settings()
        accelerometerBMI160Data.removeAll()

        let signal = mbl_mw_acc_bosch_get_acceleration_data_signal(device.board)!
        mbl_mw_datasignal_subscribe(signal, bridge(obj: self)) { (context, obj) in
            let acceleration: MblMwCartesianFloat = obj!.pointee.valueAs()
            let _self: DeviceDetailViewController = bridge(ptr: context!)
            DispatchQueue.main.async {
                _self.accelerometerBMI160Graph.addX(Double(acceleration.x), y: Double(acceleration.y), z: Double(acceleration.z))
            }
            // Add data to data array for saving
            _self.accelerometerBMI160Data.append((obj!.pointee.epoch, acceleration))
        }
        mbl_mw_acc_enable_acceleration_sampling(device.board)
        mbl_mw_acc_start(device.board)
        
        streamingCleanup[signal] = {
            mbl_mw_acc_stop(self.device.board)
            mbl_mw_acc_disable_acceleration_sampling(self.device.board)
            mbl_mw_datasignal_unsubscribe(signal)
        }
    }

    @IBAction func accelerometerBMI160StopStreamPressed(_ sender: Any) {
        accelerometerBMI160StartStream.isEnabled = true
        accelerometerBMI160StopStream.isEnabled = false
        accelerometerBMI160StartLog.isEnabled = true        
        let signal = mbl_mw_acc_bosch_get_acceleration_data_signal(device.board)!
        streamingCleanup.removeValue(forKey: signal)?()
    }

    @IBAction func accelerometerBMI160StartLogPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func accelerometerBMI160StopLogPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func accelerometerBMI160EmailDataPressed(_ sender: Any) {
        print("no need")
    }
    
    @IBAction func accelerometerBMI160StartOrientPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func accelerometerBMI160StopOrientPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func accelerometerBMI160StartStepPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func accelerometerBMI160StopStepPressed(_ sender: Any) {
        print("no need")
    }

    func updateGyroBMI160Settings() {
        switch self.gyroBMI160Scale.selectedSegmentIndex {
            case 0:
                mbl_mw_gyro_bmi160_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_125dps)
                self.gyroBMI160Graph.fullScale = 1
            case 1:
                mbl_mw_gyro_bmi160_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_250dps)
                self.gyroBMI160Graph.fullScale = 2
            case 2:
                mbl_mw_gyro_bmi160_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_500dps)
                self.gyroBMI160Graph.fullScale = 4
            case 3:
                mbl_mw_gyro_bmi160_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_1000dps)
                self.gyroBMI160Graph.fullScale = 8
            case 4:
                mbl_mw_gyro_bmi160_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_2000dps)
                self.gyroBMI160Graph.fullScale = 16
            default:
                fatalError("Unexpected gyroBMI160Scale value")
        }
        switch self.gyroBMI160Frequency.selectedSegmentIndex {
            case 0:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_1600Hz)
            case 1:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_800Hz)
            case 2:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_400Hz)
            case 3:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_200Hz)
            case 4:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_100Hz)
            case 5:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_50Hz)
            case 6:
                mbl_mw_gyro_bmi160_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_25Hz)
            default:
                fatalError("Unexpected gyroBMI160Frequency value")
        }
        mbl_mw_gyro_bmi160_write_config(device.board)
    }
    
    func updateGyroBMI270Settings() {
        switch self.gyroBMI160Scale.selectedSegmentIndex {
            case 0:
                mbl_mw_gyro_bmi270_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_125dps)
                self.gyroBMI160Graph.fullScale = 1
            case 1:
                mbl_mw_gyro_bmi270_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_250dps)
                self.gyroBMI160Graph.fullScale = 2
            case 2:
                mbl_mw_gyro_bmi270_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_500dps)
                self.gyroBMI160Graph.fullScale = 4
            case 3:
                mbl_mw_gyro_bmi270_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_1000dps)
                self.gyroBMI160Graph.fullScale = 8
            case 4:
                mbl_mw_gyro_bmi270_set_range(device.board, MBL_MW_GYRO_BOSCH_RANGE_2000dps)
                self.gyroBMI160Graph.fullScale = 16
            default:
                fatalError("Unexpected gyroBMI160Scale value")
        }
        switch self.gyroBMI160Frequency.selectedSegmentIndex {
            case 0:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_1600Hz)
            case 1:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_800Hz)
            case 2:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_400Hz)
            case 3:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_200Hz)
            case 4:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_100Hz)
            case 5:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_50Hz)
            case 6:
                mbl_mw_gyro_bmi270_set_odr(device.board, MBL_MW_GYRO_BOSCH_ODR_25Hz)
            default:
                fatalError("Unexpected gyroBMI160Frequency value")
        }
        mbl_mw_gyro_bmi270_write_config(device.board)
    }

    @IBAction func gyroBMI160StartStreamPressed(_ sender: Any) {
        print("no need")
    }
    
    func stopGyroStream() {
        print("no need")
    }

    @IBAction func gyroBMI160StopStreamPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func gyroBMI160StartLogPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func gyroBMI160StopLogPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func gyroBMI160EmailDataPressed(_ sender: Any) {
        print("no need")
    }

    func updateSensorFusionSettings() {
        mbl_mw_sensor_fusion_set_acc_range(device.board, MBL_MW_SENSOR_FUSION_ACC_RANGE_16G)
        mbl_mw_sensor_fusion_set_gyro_range(device.board, MBL_MW_SENSOR_FUSION_GYRO_RANGE_2000DPS)
        mbl_mw_sensor_fusion_set_mode(device.board, MblMwSensorFusionMode(UInt32(sensorFusionMode.selectedSegmentIndex + 1)))
        sensorFusionMode.isEnabled = false
        sensorFusionOutput.isEnabled = false
//        sensorFusionData = Data()
//        sensorFusionGraph.fullScale = 2
    }
    
    @IBAction func resetSensorFusionPressed(_ sender: Any) {
        sensorFusionData.removeAll()
    }
    
    func setLedColor(_ color: MblMwLedColor) {
        var pattern = MblMwLedPattern(high_intensity: 31,
                                      low_intensity: 31,
                                      rise_time_ms: 0,
                                      high_time_ms: 2000,
                                      fall_time_ms: 0,
                                      pulse_duration_ms: 2000,
                                      delay_time_ms: 0,
                                      repeat_count: 0xFF)
        mbl_mw_led_stop_and_clear(device.board)
        mbl_mw_led_write_pattern(device.board, &pattern, color)
        mbl_mw_led_play(device.board)
    }

    let humanActivityManager : HumanActivityManager = HumanActivityManager()
    
    @IBAction func labelPickGesture(_ sender: Any) {
        humanActivityManager.labelGesture(gesture: "pick")
    }
    @IBAction func labelPutbackGesture(_ sender: Any) {
        humanActivityManager.labelGesture(gesture: "putback")
    }
    
    func startIphoneAccelerometer() {
        manager.accelerometerUpdateInterval = 0.02
        manager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let trueData = data {
                let mag = sqrt(pow(trueData.acceleration.x, 2.0) + pow(trueData.acceleration.y, 2.0) + pow(trueData.acceleration.z, 2.0))
                if self.humanActivityManager.getWristDataCount() >= self.humanActivityManager.getPhoneDataCount() {
                    let MA_mag = Float(mag)
                    self.humanActivityManager.phoneNext(MA_mag)
                }
            }
        }
    }
    
    func stopIphoneAccelerometer() {
        manager.stopAccelerometerUpdates()
    }
    
    var warmup_device_countdown = 10
    
    @IBAction func sensorFusionStartStreamPressed(_ sender: Any) {
        humanActivityManager.startTimer()
        sensorFusionStartStream.isEnabled = false
        sensorFusionStopStream.isEnabled = true
        sensorFusionStartLog.isEnabled = false
        sensorFusionStopLog.isEnabled = false
        updateSensorFusionSettings()
//        sensorFusionData.removeAll()
        
        switch sensorFusionOutput.selectedSegmentIndex {
        case 0:
            print("no need")
        case 1:
//            sensorFusionGraph.hasW = true
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_QUATERNION)!
            mbl_mw_datasignal_subscribe(signal, bridge(obj: self)) { (context, obj) in
                let quaternion: MblMwQuaternion = obj!.pointee.valueAs()
                let _self: DeviceDetailViewController = bridge(ptr: context!)
//                DispatchQueue.main.async {
//                    _self.sensorFusionGraph.addX(_self.sensorFusionGraph.scale(Double(quaternion.x), min: -1.0, max: 1.0),
//                                                                     y: _self.sensorFusionGraph.scale(Double(quaternion.y), min: -1.0, max: 1.0),
//                                                                     z: _self.sensorFusionGraph.scale(Double(quaternion.z), min: -1.0, max: 1.0),
//                                                                     w: _self.sensorFusionGraph.scale(Double(quaternion.w), min: -1.0, max: 1.0))
//                }
                if _self.warmup_device_countdown < 0 {
                    _self.humanActivityManager.wristbandStreamNext(type: "q", val: [quaternion.w, quaternion.x, quaternion.y, quaternion.z])
//                    _self.sensorFusionData.append("q,\(quaternion.w),\(quaternion.x),\(quaternion.y),\(quaternion.z)\n".data(using: String.Encoding.utf8)!)
                } else if _self.warmup_device_countdown == 0 {
                    _self.setLedColor(MBL_MW_LED_COLOR_GREEN)
                }
                _self.warmup_device_countdown -= 1
            }
            mbl_mw_sensor_fusion_clear_enabled_mask(device.board)
            mbl_mw_sensor_fusion_enable_data(device.board, MBL_MW_SENSOR_FUSION_DATA_QUATERNION)
            mbl_mw_sensor_fusion_write_config(device.board)
            mbl_mw_sensor_fusion_start(device.board)
            
            streamingCleanup[signal] = {
                mbl_mw_sensor_fusion_stop(self.device.board)
                mbl_mw_sensor_fusion_clear_enabled_mask(self.device.board)
                mbl_mw_datasignal_unsubscribe(signal)
            }
            
            mbl_mw_acc_bosch_set_range(device.board, MBL_MW_ACC_BOSCH_RANGE_2G)
            // set_odr: Sets the output data rate
            mbl_mw_acc_set_odr(device.board, 50.0)
            mbl_mw_acc_bosch_write_acceleration_config(device.board)
            accelerometerBMI160Data.removeAll()
            let acc_signal = mbl_mw_acc_bosch_get_acceleration_data_signal(device.board)!
            mbl_mw_datasignal_subscribe(acc_signal, bridge(obj: self)) { (context, obj) in
                let acceleration: MblMwCartesianFloat = obj!.pointee.valueAs()
                let _self: DeviceDetailViewController = bridge(ptr: context!)
                // Add data to data array for saving
                let w = sqrt(pow(acceleration.x, 2.0) + pow(acceleration.y, 2.0) + pow(acceleration.z, 2.0))
                if _self.warmup_device_countdown < 0 {
                    let MA_mag = Float(w)
                    _self.humanActivityManager.wristbandNext(MA_mag)
                    _self.humanActivityManager.wristbandStreamNext(type: "a", val: [w, acceleration.x, acceleration.y, acceleration.z])
//                        _self.sensorFusionData.append("a,\(MA_mag),\(acceleration.x),\(acceleration.y),\(acceleration.z)\n".data(using: String.Encoding.utf8)!)
                }
            }
            mbl_mw_acc_start(device.board)

            streamingCleanup[signal] = {
                mbl_mw_acc_stop(self.device.board)
                mbl_mw_acc_disable_acceleration_sampling(self.device.board)
                mbl_mw_datasignal_unsubscribe(signal)
            }
            startIphoneAccelerometer()
        case 2:
            print("no need")
        case 3:
            sensorFusionData.append("type,w,x,y,z\n".data(using: String.Encoding.utf8)!)
            sensorFusionGraph.hasW = false
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_LINEAR_ACC)!
            mbl_mw_datasignal_subscribe(signal, bridge(obj: self)) { (context, obj) in
                let acc: MblMwCartesianFloat = obj!.pointee.valueAs()
                let _self: DeviceDetailViewController = bridge(ptr: context!)
//                DispatchQueue.main.async {
//                    _self.sensorFusionGraph.addX(Double(acc.x), y: Double(acc.y), z: Double(acc.z))
//                }
                _self.sensorFusionData.append("linearAcc,0,\(acc.x),\(acc.y),\(acc.z)\n".data(using: String.Encoding.utf8)!)
            }
            mbl_mw_sensor_fusion_set_acc_range(device.board, MBL_MW_SENSOR_FUSION_ACC_RANGE_8G)
            mbl_mw_sensor_fusion_clear_enabled_mask(device.board)
            mbl_mw_sensor_fusion_enable_data(device.board, MBL_MW_SENSOR_FUSION_DATA_LINEAR_ACC)
            mbl_mw_sensor_fusion_write_config(device.board)
            mbl_mw_sensor_fusion_start(device.board)
            
            streamingCleanup[signal] = {
                mbl_mw_sensor_fusion_stop(self.device.board)
                mbl_mw_sensor_fusion_clear_enabled_mask(self.device.board)
                mbl_mw_datasignal_unsubscribe(signal)
            }
        default:
            assert(false, "Added a new sensor fusion output?")
        }
        
//        updateGyroBMI160Settings()
//        let signal = mbl_mw_gyro_bmi160_get_rotation_data_signal(device.board)!
//        mbl_mw_datasignal_log(signal, bridge(obj: self)) { (context, logger) in
//            let _self: DeviceDetailViewController = bridge(ptr: context!)
//            let cString = mbl_mw_logger_generate_identifier(logger)!
//            let identifier = String(cString: cString)
//            _self.loggers[identifier] = logger!
//        }
//        mbl_mw_logging_start(device.board, 0)
//        mbl_mw_gyro_bmi160_enable_rotation_sampling(device.board)
//        mbl_mw_gyro_bmi160_start(device.board)
    }

    @IBAction func sensorFusionStopStreamPressed(_ sender: Any) {
        sensorFusionStartStream.isEnabled = true
        sensorFusionStopStream.isEnabled = false
//        sensorFusionStartLog.isEnabled = true
        sensorFusionStartLog.isEnabled = false  // close temporarily
        sensorFusionMode.isEnabled = true
        sensorFusionOutput.isEnabled = true
        
        switch sensorFusionOutput.selectedSegmentIndex {
        case 0:
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_EULER_ANGLE)!
            streamingCleanup.removeValue(forKey: signal)?()
        case 1:
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_QUATERNION)!
            let acc_signal = mbl_mw_acc_bosch_get_acceleration_data_signal(device.board)!
            streamingCleanup.removeValue(forKey: signal)?()
            streamingCleanup.removeValue(forKey: acc_signal)?()
            stopIphoneAccelerometer()
            warmup_device_countdown = 10
            
            sensorFusionData.append("type,w,x,y,z\n".data(using: String.Encoding.utf8)!)
            sensorFusionData.append(humanActivityManager.get_all_data())
//            send(sensorFusionData, title: "SensorFusion")
//            humanActivityManager.canSendData = false
//            sensorFusionData.removeAll()
            
        case 2:
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_GRAVITY_VECTOR)!
            streamingCleanup.removeValue(forKey: signal)?()
        case 3:
            let signal = mbl_mw_sensor_fusion_get_data_signal(device.board, MBL_MW_SENSOR_FUSION_DATA_LINEAR_ACC)!
            streamingCleanup.removeValue(forKey: signal)?()
        default:
            assert(false, "Added a new sensor fusion output?")
        }
        mbl_mw_led_stop_and_clear(device.board)
    }

    @IBAction func sensorFusionStartLogPressed(_ sender: Any) {
        print("no need")
    }

    @IBAction func sensorFusionStopLogPressed(_ sender: Any) {
        print("no need")
    }
    
    @IBAction func sensorFusionSendDataPressed(_ sender: Any) {
//        sensorFusionData.append(gyroData)
//        gyroData.removeAll()
        send(sensorFusionData, title: "SensorFusion", toWhere: "custom")
//        sensorFusionData.removeAll()
    }
}

extension DeviceDetailViewController: DFUProgressDelegate {
    func dfuProgressDidChange(for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        hud?.progress = Float(progress) / 100.0
    }
}
