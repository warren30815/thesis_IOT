### Requirements
- [MetaWear board](https://mbientlab.com/store/)
- [Apple ID](https://appleid.apple.com/), you can now get started for free!  Once you are ready to submit an App to the App Store, you need a paid [Apple Developer Account](https://developer.apple.com/programs/ios/).
- Device running iOS 10.0 or later with Bluetooth 4.0/5.0 (iOS 13+, XCODE12+, BLE5.0 recommended)

> REQUIREMENT NOTES  
The iOS simulator doesn’t support Bluetooth 4.0/5.0, so test apps must be run on a real iOS device which requires a developer account.  Bluetooth 4.0 available on iPhone 4S+, iPad 3rd generation+, or iPod Touch 5th generation.

*BLUETOOTH IS NOT SUPPORTED IN THE SIMULATOR*

## Getting Started

### Pre-Installation

#### Xcode
You need to be proficient with [Xcode](https://developer.apple.com/xcode/) development to use these APIs.

#### Swift
Our APIs are written and supported in [Swift](https://developer.apple.com/swift/).

#### CocoaPods
[CocoaPods](https://cocoapods.org/) is a dependency manager for Swift and Objective-C Cocoa projects. 

```sh
sudo gem install cocoapods
```
### Installation
在terminal中切到ExampleApp資料夾，然後打下面的指令

```sh
pod install
```

然後打開所生成的MetaWearApp.xcworkspace即可

### Tutorials
Tutorials can be found [here](https://mbientlab.com/tutorials/).

### app畫面圖
<img src="./../app_ui.png" style="zoom:10%" />

### 如何紀錄data
將手機與手環連線後，在app上按圖上的start streaming按鈕，紀錄完後按stop streaming，然後按send data將所錄製的csv檔傳出來

### 如何label data
目前只能拿兩台手機&兩隻手環with MMRL，一組手機手環用來記錄，另一組用來label，將兩組的手機同時按start streaming，按label組手機app上的label pick or label putback按鈕

按下按鈕後會把label寫到label組的csv內，紀錄組的csv不會有label資訊，需手動複製過去

### 如何錄製新手勢
在app上按圖上的start streaming按鈕後，開始做手勢，做完後按stop streaming，然後按send data將所錄製的csv檔傳出來

將csv改檔名，移到python那邊的template_data/資料夾

檔名取名格式為SensorFusion_template_[手勢名稱][編號].csv，可參考template_data/內的命名

