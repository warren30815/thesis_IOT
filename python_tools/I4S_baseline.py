from typing import List

import antropy as ent
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
from tqdm import trange
import copy
from tool import *

DATA_TYPE = ["acc", "q"]
GESTURE_LIST = ["pick", "putback"]

def read_acc_comparison_data(data_index, signal_data_path):
    signal = {'acc': {'x': [], 'y': [], 'z': [], 'mag': []}, 'q': {'x': [], 'y': [], 'z': [], 'mag': []}}
    # 哪些data index是身體靜止時紀錄的
    # 在算Locomotion時，如果這個frame有任何一個index落在hand_motion_index_set內，
    # 則Locomotion = 0, 反之 = 1
    hand_motion_index_set = set()
    label_index_dict = {}
    for g in GESTURE_LIST:
        label_index_dict[g] = set()


    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)
        if data_type == 'm':
            relax_range = 100
            motion_index = int(w)
            start_index = max(0, motion_index - relax_range)
            end_index = motion_index + relax_range + 1
            for m in range(start_index, end_index):
                hand_motion_index_set.add(m)
        if data_type == 'l':
            label_data = w.split('-')
            # fix bugs
            # replace motion_index as time
            gesture, time = label_data[0], int(label_data[1])
            if gesture in GESTURE_LIST:
                label_gesture_dict[data_index][gesture].add(time)
                start_index = max(0, time)
                end_index = time + 1
                for t in range(start_index, end_index):
                    label_index_dict[gesture].add(t)

    return signal, hand_motion_index_set, label_index_dict


def compute_mcr(seq):
    mean = np.mean(seq)
    count = 0
    for i in range(1, len(seq)-1):
        if (seq[i] > mean and seq[i-1] <= mean) or (seq[i] <= mean and seq[i-1] > mean):
            count += 1
    return count


def compute_sub_windows_feature(x: List, y: List, z: List, window_size=20, step=10):
    """[summary]

    Args:
        x (List): [description]
        y (List): [description]
        z (List): [description]
        window_size (int, optional): [description]. Defaults to 20.
        step (int, optional): [description]. Defaults to 10.

    Returns:
        max mean:
        max rise
        max drop
    """
    assert len(x) == len(y) == len(z)

    def helper(seq):
        mean_list = []
        for start_pos in range(0, len(seq)-window_size, step):
            sub_seq = seq[start_pos:start_pos+window_size]
            mean_list.append(np.mean(sub_seq))

        diff_list = np.diff(mean_list)

        max_mean = np.max(mean_list)
        max_rise = np.max(diff_list)
        max_drop = np.min(diff_list)
        return [max_mean, max_rise, max_drop]

    ret = list(zip(helper(x), helper(y), helper(z)))
    return list(ret[0]), list(ret[1]), list(ret[2])


def compute_covariance(x, y, z):
    """compute xy, xz, yz covariance
    """
    cov_mat_xy = np.stack((x, y), axis=0)
    cov_mat_xz = np.stack((x, z), axis=0)
    cov_mat_yz = np.stack((y, z), axis=0)
    return [np.cov(cov_mat_xy)[0][1], np.cov(cov_mat_xz)[0][1], np.cov(cov_mat_yz)[0][1]]

# def get_whole_gesture_duration(label, start_pos, end_pos):
#     def condition(x): return start_pos <= x <= end_pos

#     interval = end_pos - start_pos
#     # label 0: pick
#     # label 1: putback
#     if label == 0:
#         gesture = 'pick'
#     elif label == 1:
#         gesture = 'putback'

#     output = [element for element in label_index_dict[gesture] if condition(element)]
#     pos = output[0]
#     if gesture == 'pick':
#         start_pos = pos
#         end_pos = pos + interval
#     elif gesture == 'putback':
#         start_pos = pos - interval
#         end_pos = pos
#     return start_pos, end_pos

def preproc(csv_filename, signal, hand_motion_index_set, label_index_dict, window_size=100, step=50):
    """Preprocess the data and generate the features.

    Args:
        signal ([type]): [description]
        hand_motion_index_set ([type]): [description]
        label_index_dict ([type]): [description]
        window_size (int, optional): [description]. Defaults to 100.
        step (int, optional): [description]. Defaults to 50.

    Returns:
        features (nd.array) of shape (n_smaples, n_features)
        labels (nd.array) of shape (n_samples,)
    """
    seq_length_set = set([len(v) for v in signal['acc'].values()] + [len(v) for v in signal['q'].values()])
    assert len(seq_length_set) == 1

    seq_length = seq_length_set.pop()
    features_list = []
    label_list = []
    valid_gesture_time = {}
    for g in GESTURE_LIST:
        valid_gesture_time[g] = set()
    start_pos_list = []
    all_features_list = []
    all_label_list = []
    all_start_pos_list = []
    current_pick_count =  0
    current_putback_count =  0
    current_no_gesture_count = 0

    for start_pos in trange(0, seq_length-window_size, step):
        end_pos = start_pos+window_size
        # label 0: pick
        # label 1: putback
        # label 2: none
        label = 2
        if any(i in label_index_dict['pick'] for i in range(start_pos, end_pos)):
            label = 0

        if any(i in label_index_dict['putback'] for i in range(start_pos, end_pos)):
            label = 1

        # balance each label amount
        if label == 0:
            current_pick_count += 1
        elif label == 1:
            current_putback_count += 1

        # if label != 2 and csv_filename.startswith('training'):  # won't be better
        #     start_pos, end_pos = get_whole_gesture_duration(label, start_pos, end_pos)

        acc_x = list(np.array(signal['acc']['x'][start_pos:end_pos], dtype=np.float32))
        acc_y = list(np.array(signal['acc']['y'][start_pos:end_pos], dtype=np.float32))
        acc_z = list(np.array(signal['acc']['z'][start_pos:end_pos], dtype=np.float32))
        acc_mag = list(np.array(signal['acc']['mag'][start_pos:end_pos], dtype=np.float32))

        q_x = list(np.array(signal['q']['x'][start_pos:end_pos], dtype=np.float32))
        q_y = list(np.array(signal['q']['y'][start_pos:end_pos], dtype=np.float32))
        q_z = list(np.array(signal['q']['z'][start_pos:end_pos], dtype=np.float32))
        q_mag = list(np.array(signal['q']['mag'][start_pos:end_pos], dtype=np.float32))

        mean = [np.mean(seq) for seq in [acc_x, acc_y, acc_z, acc_mag, q_x, q_y, q_z, q_mag]]

        var = [np.var(seq) for seq in [acc_x, acc_y, acc_z, q_x, q_y, q_z]]
        
        mcr = [compute_mcr(seq) for seq in [acc_x, acc_y, acc_z, q_x, q_y, q_z]]

        acc_max_mean, acc_max_rise, acc_max_drop = compute_sub_windows_feature(acc_x, acc_y, acc_z)
        q_max_mean, q_max_rise, q_max_drop = compute_sub_windows_feature(q_x, q_y, q_z)
        max_mean, max_rise, max_drop = acc_max_mean + q_max_mean, acc_max_rise + q_max_rise, acc_max_drop + q_max_drop

        cov = compute_covariance(acc_x, acc_y, acc_y) + compute_covariance(q_x, q_y, q_z)

        entropy = [ent.spectral_entropy(seq, sf=50) for seq in [acc_x, acc_y, acc_z, q_x, q_y, q_z]]

        locomation = 0 if any(i in hand_motion_index_set for i in range(
            start_pos, end_pos)) else 1

        feature = mean + var + mcr + max_mean + max_rise + max_drop + cov + entropy + [locomation]

        if label != 2:
            features_list.append(feature) 
            label_list.append(label)
            start_pos_list.append(start_pos)
        elif label == 2 and current_no_gesture_count < max(current_pick_count, current_putback_count):
            current_no_gesture_count += 1
            features_list.append(feature) 
            label_list.append(label)
            start_pos_list.append(start_pos)
            
        all_features_list.append(feature) 
        all_label_list.append(label)
        all_start_pos_list.append(start_pos)

    return np.array(features_list, dtype=np.float32), label_list, start_pos_list, np.array(all_features_list, dtype=np.float32), all_label_list, all_start_pos_list

def build_model():
    clf = RandomForestClassifier(random_state=0)
    return clf

if __name__ == '__main__':
    window_size = 100
    step = 50
    test_files = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']
    # test_files = ['SensorFusion_07_10_2021-16_20_38']
    test_file_num = len(test_files)
    training_files = ['training1', 'training2', 'training3']
    # training_files = ['training2']
    training_file_num = len(training_files)
    training_data_index_list = [i for i in range(test_file_num, test_file_num + training_file_num)]

    csv_filenames = test_files + training_files
    X_train = []
    y_train = []
    X_test_dict = {}
    label_gesture_dict = {}

    total_feature_extraction_time_dict = {'train': [], 'test': []}
    avg_training_time_list = []
    avg_inference_time_list = []
    import time

    for index, csv_filename in enumerate(csv_filenames):
        label_gesture_dict[index] = {}
        for g in GESTURE_LIST:
            label_gesture_dict[index][g] = set()
        # label_data_path = "log/%s_label.csv" % (csv_filename)
        # read_acc_comparison_data(index, label_data_path)

        signal_data_path = "../real_data/%s.csv" % (csv_filename)
        signal, hand_motion_index_set, label_index_dict = read_acc_comparison_data(index, signal_data_path)
        start_feature_extraction_time = time.time()
        features_list, label_list, start_pos_list, all_features_list, all_label_list, all_start_pos_list = preproc(csv_filename, signal, hand_motion_index_set, label_index_dict, window_size, step)
        if index in training_data_index_list:
            for feature_list in features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_train.append(feature_array)
            y_train += label_list
            total_feature_extraction_time_dict['train'].append(time.time() - start_feature_extraction_time)
        else:
            X_test_dict[index] = { 'feature_array': [], 'all_start_pos_list': [] }
            for feature_list in all_features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_test_dict[index]['feature_array'].append(feature_array)
            X_test_dict[index]['all_start_pos_list'] += all_start_pos_list
            total_feature_extraction_time_dict['test'].append((time.time() - start_feature_extraction_time) / len(all_features_list))

    dict_npy_path = 'i4s_%d_%d_X_test_dict.npy' % (window_size, step)
    X_train_array = np.array(X_train)
    y_train_array = np.array(y_train)
    np.save('i4s_%d_%d_X_train_array.npy' % (window_size, step), X_train_array)
    np.save('i4s_%d_%d_y_train_array.npy' % (window_size, step), y_train_array)
    np.save(dict_npy_path, X_test_dict)

    # X_train_array = np.load('i4s_%d_%d_X_train_array.npy' % (window_size, step))
    # y_train_array = np.load('i4s_%d_%d_y_train_array.npy' % (window_size, step))
    # X_npy = np.load(dict_npy_path, allow_pickle=True)
    # X_test_dict = X_npy.item()

    start_training_time = time.time()
    model = build_model()
    model.fit(X_train_array, y_train_array)
    avg_training_time_list.append(time.time() - start_training_time)
    prediction_dict = {}
    confidnece = 0.5
    relax_range = 150
    for data_index in X_test_dict:
        start_inference_time = time.time()
        X_test_array = np.array(X_test_dict[data_index]['feature_array'])
        prediction_dict[data_index] = []
        outputs = model.predict_proba(X_test_array)  
        for output in outputs:
            prediction_dict[data_index].append(output)
        avg_inference_time_list.append((time.time() - start_inference_time) / len(outputs))

    tp_fn_result_dict = get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_fp_num_dict, FP_dict = get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    adjacent_result_dict = get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_adjacence_fp_num = get_adjacent_pick_putback_fp(X_test_dict, FP_dict, prediction_dict, window_size, confidnece)

    print("----All----")
    for g in GESTURE_LIST:
        print("gesture: ", g)
        recall = tp_fn_result_dict[g]['success'] / tp_fn_result_dict[g]['total']
        precision = tp_fn_result_dict[g]['success'] / (tp_fn_result_dict[g]['success'] + total_fp_num_dict[g])
        print("All recall: ")
        print(tp_fn_result_dict[g]['success'], tp_fn_result_dict[g]['total'], str(recall * 100) + '%')
        print("All precision: ")
        print(tp_fn_result_dict[g]['success'], total_fp_num_dict[g], str(precision * 100) + '%')
        print("All F1-score: ")
        print(2 * precision * recall / (precision + recall))
    adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
    adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + total_adjacence_fp_num)
    print("All adjacent recall: ")
    print(adjacent_result_dict['success'], adjacent_result_dict['total'], adjacence_recall)
    print("All adjacent precision: ")
    print(adjacent_result_dict['success'], total_adjacence_fp_num, adjacence_precision)
    print("All adjacent F1-score: ")
    print(2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))
    print("total train feature_extraction_time", np.array(total_feature_extraction_time_dict['train']).sum())
    print("total test feature_extraction_time", np.array(total_feature_extraction_time_dict['test']).mean())
    print("avg_training_time", np.array(avg_training_time_list).mean())
    print("avg_inference_time", np.array(avg_inference_time_list).mean())