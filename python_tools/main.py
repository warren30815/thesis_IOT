#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from scipy import stats
from dtw_algo.dtw.dtw import dtw
import itertools
import tslearn.barycenters
from tqdm import trange
import time
from multiprocessing import Pool
import os

plot_3d_anim = False
csv_filenames = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']

TEMPLATE_SETTING = { "pick": { "template_num": 3, "need_add_std": False}, "putback": { "template_num": 3, "need_add_std": False}, "norotateputback": { "template_num": 3, "need_add_std": False},
                    "norotateputback_v2": { "template_num": 5, "need_add_std": False}, "pick_v2": { "template_num": 5, "need_add_std": True} }
GESTURE_LIST = list(TEMPLATE_SETTING.keys())

sequence_length_list = [60]
step_list = [15]
# sequence_length_list = [30, 40, 50, 60, 70, 80, 90]
# step_list = [5, 10, 15, 20, 25]  # overlap = sequence_length - step
barycenter_size = 30
DATA_TYPE = ["acc", "q"]
CHANNEL = ['x', 'y', 'z']

def read_acc_comparison_data(signal_data_path):
    signal = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }
    df = pd.read_csv(signal_data_path)
    motion_index_set = set()

    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
        if data_type == 'm':
            motion_index = int(w)
            motion_index_set.add(motion_index)

    return signal, motion_index_set

def read_templates_data():
    gesture_template_dict = {}
    for gesture in GESTURE_LIST:
        gesture_template_dict[gesture] = {}
        TEMPLATE_NUM = TEMPLATE_SETTING[gesture]["template_num"]
        for data_index in range(1,TEMPLATE_NUM+1):
            gesture_template_dict[gesture][data_index] = {}
            # print(gesture, data_index)
            data_path = "../template_data/SensorFusion_template_%s%d.csv" % (gesture, data_index)
            df = pd.read_csv(data_path)
            gesture_template_dict[gesture][data_index] = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }
            for index, row in df.iterrows():
                data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
                if data_type == 'a':
                    if len(gesture_template_dict[gesture][data_index]['q']['x']) > len(gesture_template_dict[gesture][data_index]['acc']['x']):  # to make sure one q match one a
                        gesture_template_dict[gesture][data_index]['acc']['x'].append(x)
                        gesture_template_dict[gesture][data_index]['acc']['y'].append(y)
                        gesture_template_dict[gesture][data_index]['acc']['z'].append(z)
                if data_type == 'q':
                    gesture_template_dict[gesture][data_index]['q']['x'].append(x)
                    gesture_template_dict[gesture][data_index]['q']['y'].append(y)
                    gesture_template_dict[gesture][data_index]['q']['z'].append(z)

    return gesture_template_dict

def merge_multiple_templates():
    z_norm_time_series = {}
    merged_z_norm_templates = {}

    # init
    for gesture in GESTURE_LIST:
        z_norm_time_series[gesture] = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }  # []: list of lists (2D)
        merged_z_norm_templates[gesture] = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }  # []: list (1D)
        TEMPLATE_NUM = TEMPLATE_SETTING[gesture]["template_num"]
        for template_index in range(1,TEMPLATE_NUM+1):
            for d_type in DATA_TYPE:
                for key in CHANNEL:
                    z_norm_template = stats.zscore(gesture_template_dict[gesture][template_index][d_type][key])
                    z_norm_time_series[gesture][d_type][key].append(z_norm_template)

    for gesture in GESTURE_LIST:
        for d_type in DATA_TYPE:
            for key in CHANNEL:
                merged_z_norm_templates[gesture][d_type][key] = np.array(tslearn.barycenters.dtw_barycenter_averaging(z_norm_time_series[gesture][d_type][key], barycenter_size=barycenter_size)).flatten()

    return merged_z_norm_templates 

def calculate_template_distance():
    templates_distance = {}

    for gesture in GESTURE_LIST:
        templates_distance[gesture] = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }  # []: list of float (distance)
        TEMPLATE_NUM = TEMPLATE_SETTING[gesture]["template_num"]
        for d_type in DATA_TYPE:
            for key, z_norm_merged_template in merged_z_norm_templates[gesture][d_type].items():
                for index in range(TEMPLATE_NUM):
                    z_norm_template = stats.zscore(gesture_template_dict[gesture][index+1][d_type][key])
                    x = np.array(z_norm_merged_template).reshape(-1, 1)
                    y = np.array(z_norm_template).reshape(-1, 1)
                    manhattan_distance = lambda x, y: np.abs(x - y)
                    d = dtw(x, y, dist=manhattan_distance)
                    templates_distance[gesture][d_type][key].append(d)

    combination_templates_distance = {}

    for gesture in GESTURE_LIST:
        combination_templates_distance[gesture] = { 'acc': { 'x': [], 'y': [], 'z': [] }, 'q': { 'x': [], 'y': [], 'z': [] } }  # []: list of float (distance)
        TEMPLATE_NUM = TEMPLATE_SETTING[gesture]["template_num"]
        all_combinations = list(itertools.combinations(list(range(1,TEMPLATE_NUM+1)), 2))
        for template_index_pair in all_combinations:
            for d_type in DATA_TYPE:
                for key in CHANNEL:
                    template_index1 = template_index_pair[0]
                    template_index2 = template_index_pair[1]
                    z_norm_template_signal1 = stats.zscore(gesture_template_dict[gesture][template_index1][d_type][key])
                    z_norm_template_signal2 = stats.zscore(gesture_template_dict[gesture][template_index2][d_type][key])
                    x = np.array(z_norm_template_signal1).reshape(-1, 1)
                    y = np.array(z_norm_template_signal2).reshape(-1, 1)
                    manhattan_distance = lambda x, y: np.abs(x - y)
                    d = dtw(x, y, dist=manhattan_distance)
                    combination_templates_distance[gesture][d_type][key].append(d)

    return templates_distance, combination_templates_distance

def calculate_distance_threshold():
    distance_threshold = {}
    for gesture in GESTURE_LIST:
        distance_threshold[gesture] = { 'acc': { 'x': {}, 'y': {}, 'z': {} }, 'q': { 'x': {}, 'y': {}, 'z': {} } }  # {}: min value, max value

        # [Region] get combination upper
        for d_type in DATA_TYPE:
            for t_key, t_value in combination_templates_distance[gesture][d_type].items():
                mean = np.mean(t_value)
                std = np.std(t_value)
                distance_threshold[gesture][d_type][t_key]["c_upper"] = mean 
                NEED_STD = TEMPLATE_SETTING[gesture]["need_add_std"]
                if mean <= 20 and NEED_STD:
                    distance_threshold[gesture][d_type][t_key]["c_upper"] += 1 * std
        # [EngRegion]

        for d_type in DATA_TYPE:
            for t_key, t_value in templates_distance[gesture][d_type].items():
                _max = np.max(t_value)
                mean = np.mean(t_value)
                std = np.std(t_value)
                distance_threshold[gesture][d_type][t_key]["min"] = 0
                distance_threshold[gesture][d_type][t_key]["max"] = _max
                # cuz our templates amount is finite, so we add extra relaxed value to threshold
                if mean < 10:
                    distance_threshold[gesture][d_type][t_key]["max"] += mean * 2.25
                else:
                    distance_threshold[gesture][d_type][t_key]["max"] += mean

                if distance_threshold[gesture][d_type][t_key]["max"] > distance_threshold[gesture][d_type][t_key]["c_upper"]:
                    distance_threshold[gesture][d_type][t_key]["max"] = distance_threshold[gesture][d_type][t_key]["c_upper"]
                print("gesture: %s, key: %s, %s template max distance: %f" % (gesture, t_key, d_type, distance_threshold[gesture][d_type][t_key]["max"]))
    return distance_threshold

# for parallel
def calculate_dtw_distance(_list):
    gesture, d_type, channel, start_index, end_index = _list[0], _list[1], _list[2], _list[3], _list[4]
    z_norm_template_signal = merged_z_norm_templates[gesture][d_type][channel]
    value = signal[d_type][channel]
    value_part = value[start_index: end_index]
    z_score = stats.zscore(value_part)
    x = np.array(z_score)
    y = np.array(z_norm_template_signal)
    manhattan_distance = lambda x, y: np.abs(x - y)
    d = dtw(x, y, dist=manhattan_distance)
    return gesture + '-' + d_type + '-' + channel + '-' + str(d)

# [Region] init templates
gesture_template_dict = read_templates_data()
merged_z_norm_templates = merge_multiple_templates()
templates_distance, combination_templates_distance = calculate_template_distance()
distance_threshold = calculate_distance_threshold()
# [EndRegion]

for sequence_length in sequence_length_list:
    for step in step_list:
        for csv_filename in csv_filenames:
            print("csv_filename", csv_filename)
            signal_data_path = "../real_data/%s.csv" % (csv_filename)
            signal, motion_index_set = read_acc_comparison_data(signal_data_path)

            MIN_MATCH_LEN = {}
            for g in GESTURE_LIST:
                MIN_MATCH_LEN[g] = {}
                for d_type in DATA_TYPE:
                    MIN_MATCH_LEN[g][d_type] = 2

            gesture_detection_dict = {}  # key: start and end index, value: time for match distance threshold range

            signal_length = min(len(signal['q']['x']), len(signal['acc']['x']))
            pool = Pool(processes=os.cpu_count())
            for start_index in trange(0, signal_length-sequence_length, step):
                if start_index in motion_index_set:
                    end_index = min(start_index+sequence_length, signal_length)

                    parallel_data = []
                    for gesture in GESTURE_LIST:
                        for d_type in DATA_TYPE:
                            for channel in CHANNEL:
                                parallel_data.append((gesture, d_type, channel, start_index, end_index))

                    # start_time = time.time()
                    distance_list = pool.imap_unordered(calculate_dtw_distance, parallel_data)
                    for distance_info in distance_list:
                        distance_info_list = distance_info.split('-')
                        gesture = distance_info_list[0]
                        d_type = distance_info_list[1]
                        channel = distance_info_list[2]
                        distance = float(distance_info_list[3])
                        if distance <= distance_threshold[gesture][d_type][channel]["max"]:
                            dict_key = gesture + "-" + str(start_index) + "-" + str(end_index)
                            if dict_key in gesture_detection_dict:
                                gesture_detection_dict[dict_key].append(d_type + '-' + channel)
                            else:
                                gesture_detection_dict[dict_key] = [d_type + '-' + channel]
                    # print("cost: ", time.time() - start_time)

            outputFile = open("log/seq=%d_step=%d_bary=%d_%s_result.txt" % (sequence_length, step, barycenter_size, csv_filename), "w")
            print("---")
            for index, (key, value) in enumerate(gesture_detection_dict.items()):
                acc_match_len = len([i for i in value if i.startswith('acc')])
                q_match_len = len([i for i in value if i.startswith('q')])
                gesture, start, end = key.split('-')[0], key.split('-')[1], key.split('-')[2]
                if acc_match_len + q_match_len >= 4:
                    print(key)
                    print(value)
                    outputFile.write('key: ' + key)
                    outputFile.write("\n")
                    outputFile.write('value: ' + str(value))
                    outputFile.write("\n")