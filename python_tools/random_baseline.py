import numpy as np
import pandas as pd
from tqdm import trange
import copy
from tool import *

GESTURE_LIST = ["pick", "putback"]

def read_acc_comparison_data(data_index, signal_data_path):
    signal = {'acc': {'x': [], 'y': [], 'z': [], 'mag': []}, 'q': {'x': [], 'y': [], 'z': [], 'mag': []}}
    label_index_dict = {}
    for g in GESTURE_LIST:
        label_index_dict[g] = set()


    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)
        if data_type == 'l':
            label_data = w.split('-')
            # fix bugs
            # replace motion_index as time
            gesture, time = label_data[0], int(label_data[1])
            if gesture in GESTURE_LIST:
                label_gesture_dict[data_index][gesture].add(time)
                start_index = max(0, time)
                end_index = time + 1
                for t in range(start_index, end_index):
                    label_index_dict[gesture].add(t)

    return signal, label_index_dict

def preproc(signal, label_index_dict, window_size=100, step=50):
    seq_length = len(signal['acc']['x'])
    all_start_pos_list = []

    for start_pos in trange(0, seq_length-window_size, step):
        # label 0: pick
        # label 1: putback
        # label 2: none
        label = 2
        if any(i in label_index_dict['pick'] for i in range(start_pos, start_pos+window_size)):
            label = 0

        if any(i in label_index_dict['putback'] for i in range(start_pos, start_pos+window_size)):
            label = 1

        all_start_pos_list.append(start_pos)

    return all_start_pos_list

if __name__ == '__main__':
    SAMPLE_TIME = 100
    recall_dict = {}
    precision_dict = {}
    for g in GESTURE_LIST:
        recall_dict[g] = []
        precision_dict[g] = []
    recall_dict['adjacence'] = []
    precision_dict['adjacence'] = []

    for _ in range(SAMPLE_TIME):
        window_size = 100
        step = 50
        test_files = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                     'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                    'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                    'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                    'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                    'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']
        out_test_files = ['out_' + name for name in test_files]
        test_file_num = len(test_files)
        csv_filenames = test_files

        csv_filenames = out_test_files
        X_test_dict = {}
        label_gesture_dict = {}
        for index, csv_filename in enumerate(csv_filenames):
            label_gesture_dict[index] = {}
            for g in GESTURE_LIST:
                label_gesture_dict[index][g] = set()
            # label_data_path = "log/%s_label.csv" % (csv_filename)
            # read_acc_comparison_data(index, label_data_path)

            signal_data_path = "../real_data/%s.csv" % (csv_filename)
            signal, label_index_dict = read_acc_comparison_data(index, signal_data_path)
            all_start_pos_list = preproc(signal, label_index_dict, window_size, step)
           
            X_test_dict[index] = { 'all_start_pos_list': [] }
            X_test_dict[index]['all_start_pos_list'] += all_start_pos_list

        dict_npy_path = 'random_%d_%d_X_test_dict.npy' % (window_size, step)
        np.save(dict_npy_path, X_test_dict)

        # X_npy = np.load(dict_npy_path, allow_pickle=True)
        # X_test_dict = X_npy.item()

        prediction_dict = {}
        confidnece = 0.5
        relax_range = 150
        for data_index in X_test_dict:
            all_start_pos_list = np.array(X_test_dict[data_index]['all_start_pos_list'])
            prediction_dict[data_index] = []
            for _ in all_start_pos_list:
                prediction = np.random.choice([0,1,2])
                output = []
                for i in range(3):
                    if i == prediction:
                        output.append(1)
                    else:
                        output.append(0)
                prediction_dict[data_index].append(output)

        tp_fn_result_dict = get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
        total_fp_num_dict, FP_dict = get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
        adjacent_result_dict = get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
        total_adjacence_fp_num = get_adjacent_pick_putback_fp(X_test_dict, FP_dict, prediction_dict, window_size, confidnece)

        for g in GESTURE_LIST:
            recall = tp_fn_result_dict[g]['success'] / tp_fn_result_dict[g]['total']
            precision = tp_fn_result_dict[g]['success'] / (tp_fn_result_dict[g]['success'] + total_fp_num_dict[g])
            recall_dict[g].append(recall)
            precision_dict[g].append(precision)
        adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
        adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + total_adjacence_fp_num)
        recall_dict['adjacence'].append(adjacence_recall)
        precision_dict['adjacence'].append(adjacence_precision)


    print("----All----")
    for g in GESTURE_LIST:
        print("gesture: ", g)
        recall = sum(recall_dict[g]) / len(recall_dict[g])
        precision = sum(precision_dict[g]) / len(precision_dict[g])
        print("All recall: ")
        print(str(recall * 100) + '%')
        print("All precision: ")
        print(str(precision * 100) + '%')
        print("All F1-score: ")
        print(2 * precision * recall / (precision + recall))
    adjacence_recall = sum(recall_dict['adjacence']) / len(recall_dict['adjacence'])
    adjacence_precision = sum(precision_dict['adjacence']) / len(precision_dict['adjacence'])
    print("All adjacent recall: ")
    print(adjacence_recall)
    print("All adjacent precision: ")
    print(adjacence_precision)
    print("All adjacent F1-score: ")
    print(2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))