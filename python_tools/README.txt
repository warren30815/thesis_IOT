main.py: 本論文方法，sliding window和手勢辨識寫在裡面

*_baseline.py: 各baseline code，會直接吐出independent & continuous F1

analyze.py: 計算本論文方法結果（independent & continuous F1）的檔案，後處理也寫在裡面，主要是吃main.py 跑出來的txt 

log/: 存放結果的資料夾，裡面的_label.csv結尾的檔案為方便analyze.py算結果用的，可以不用動它

dtw_algo/: 開源dtw演算法，可以不用動它

per_user_scatter_plot.py: 畫3D scatter plot，實驗Per-User F1-score的圖

tool.py: 裡面主要放給baseline算結果的function

requirements.txt: 本論文方法實作用到的套件，可用pip3 install -r requirements.txt來安裝


執行流程：
pip3 install -r requirements.txt
python3 main.py
python3 analyze.py