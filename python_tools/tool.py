# tp, fn
def get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range):
    import numpy as np
    gesture_list = ['pick', 'putback']
    result_dict = {}
    for g in gesture_list:
        result_dict[g] = {'total': 0, 'success': 0}

    for data_index in X_test_dict:
        all_start_pos_list = X_test_dict[data_index]['all_start_pos_list']
        for gesture in gesture_list:
            timeslots = label_gesture_dict[data_index][gesture]
            for timeslot in timeslots:
                if gesture == "pick":
                    for pos_index, start_pos in enumerate(all_start_pos_list):
                        prediction = prediction_dict[data_index][pos_index]
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= start_pos + window_size <= timeslot + relax_range):    
                            max_index = np.argmax(prediction)
                            if max_index == 0:  # 0: pick
                                if prediction[max_index] > confidnece:
                                    result_dict['pick']['success'] += 1
                                    break
                    result_dict['pick']['total'] += 1
                elif gesture == "putback":
                    for pos_index, start_pos in enumerate(all_start_pos_list):
                        prediction = prediction_dict[data_index][pos_index]
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= start_pos + window_size <= timeslot + relax_range):
                            max_index = np.argmax(prediction)
                            if max_index == 1:  # 1: putback
                                if prediction[max_index] > confidnece:
                                    result_dict['putback']['success'] += 1
                                    break
                    result_dict['putback']['total'] += 1
    return result_dict

def get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range):
    import numpy as np
    gesture_list = ['pick', 'putback']
    total_fp_num_dict = {}
    for g in gesture_list:
        total_fp_num_dict[g] = 0
    result_dict = {}

    for data_index in X_test_dict:
        result_dict[data_index] = {}
        for g in gesture_list:
            result_dict[data_index][g] = { 'start': [] }

        all_start_pos_list = X_test_dict[data_index]['all_start_pos_list']
        last_pick_FP_flag = False
        last_putback_FP_flag = False
        for pos_index, start_pos in enumerate(all_start_pos_list):
            prediction = prediction_dict[data_index][pos_index]
            max_index = np.argmax(prediction)
            if max_index == 0:  # 0: pick
                timeslots = label_gesture_dict[data_index]['pick']
                if prediction[max_index] > confidnece:
                    FP_flag = True
                    for timeslot in timeslots:
                        end_pos = start_pos + window_size
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):  
                            FP_flag = False
                            break
                    if FP_flag == True and last_pick_FP_flag == False:
                        total_fp_num_dict['pick'] += 1
                        result_dict[data_index]['pick']['start'].append(start_pos)
                else:
                    FP_flag = False
                last_pick_FP_flag = FP_flag
            elif max_index == 1:  # 1: putback
                timeslots = label_gesture_dict[data_index]['putback']
                if prediction[max_index] > confidnece:
                    FP_flag = True
                    for timeslot in timeslots:
                        end_pos = start_pos + window_size
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):  
                            FP_flag = False
                            break
                    if FP_flag == True and last_putback_FP_flag == False:
                        total_fp_num_dict['putback'] += 1
                        result_dict[data_index]['putback']['start'].append(start_pos)
                else:
                    FP_flag = False
                last_putback_FP_flag = FP_flag

    return total_fp_num_dict, result_dict

def get_adjacent_pick_putback_label(individual_file_label_gesture_dict):
    adjacent_list = []
    pick_timeslots = sorted(individual_file_label_gesture_dict["pick"])
    putback_timeslots = sorted(individual_file_label_gesture_dict["putback"])
    pick_length = len(pick_timeslots)
    putback_length = len(putback_timeslots)
    current_putback_pointer = 0
    current_pick_pointer = 0
    for pick_pointer in range(pick_length - 1):
        current_pick_pointer = pick_pointer
        pick_timeslot = pick_timeslots[pick_pointer]
        next_pick_timeslot = pick_timeslots[pick_pointer+1]
        putback_timeslot = putback_timeslots[current_putback_pointer]
        if putback_timeslot < next_pick_timeslot:
            adjacent_list.append([pick_timeslot, putback_timeslot])
            while current_putback_pointer < putback_length and putback_timeslots[current_putback_pointer] < next_pick_timeslot:  # 多個putback連續發生
                current_putback_pointer += 1

        if current_putback_pointer >= putback_length:
            break

    for remain_putback_pointer in range(current_putback_pointer, putback_length):
        remain_putback_timeslot = putback_timeslots[remain_putback_pointer]
        for pick_pointer in range(pick_length - 1):
            pick_timeslot = pick_timeslots[pick_pointer]
            next_pick_timeslot = pick_timeslots[pick_pointer+1]
            if pick_timeslot < remain_putback_timeslot < next_pick_timeslot:
                adjacent_list.append([pick_timeslot, remain_putback_timeslot])
                break
            if pick_pointer + 1 == pick_length - 1:
                if next_pick_timeslot < remain_putback_timeslot:
                    adjacent_list.append([next_pick_timeslot, remain_putback_timeslot])
                    break
    return adjacent_list

def get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range):
    import numpy as np
    gesture_list = ['pick', 'putback']
    result_dict = {'total': 0, 'success': 0, 'pickTP': [], 'putbackTP': []}

    for data_index in X_test_dict:
        result_dict['pickTP'] = []
        result_dict['putbackTP'] = []
        all_start_pos_list = X_test_dict[data_index]['all_start_pos_list']

        # get TP
        for gesture in gesture_list:
            timeslots = sorted(label_gesture_dict[data_index][gesture])
            for timeslot in timeslots:
                if gesture == "pick":
                    for pos_index, start_pos in enumerate(all_start_pos_list):
                        end_pos = start_pos + window_size
                        prediction = prediction_dict[data_index][pos_index]
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):    
                            max_index = np.argmax(prediction)
                            if max_index == 0:  # 0: pick
                                if prediction[max_index] > confidnece:
                                    result_dict['pickTP'].append(start_pos)
                                    break
                elif gesture == "putback":
                    for pos_index, start_pos in enumerate(all_start_pos_list):
                        end_pos = start_pos + window_size
                        prediction = prediction_dict[data_index][pos_index]
                        if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):    
                            max_index = np.argmax(prediction)
                            if max_index == 1:  # 1: putback
                                if prediction[max_index] > confidnece:
                                    result_dict['putbackTP'].append(start_pos)
                                    break

        adjacent_list = get_adjacent_pick_putback_label(label_gesture_dict[data_index])

        result_dict['total'] += len(adjacent_list)
        # calculate accuracy
        for adjacent_pair in adjacent_list:
            pick_timeslot = adjacent_pair[0]
            putback_timeslot = adjacent_pair[1]
            pick_TP_start_pos_list = result_dict['pickTP']
            putback_TP_start_pos_list = result_dict['putbackTP']
            pick_flag = False
            putback_flag = False
            for pick_TP_start_pos in pick_TP_start_pos_list:
                if (pick_timeslot - relax_range <= pick_TP_start_pos <= pick_timeslot + relax_range) or (pick_timeslot - relax_range <= pick_TP_start_pos + window_size <= pick_timeslot + relax_range):    
                    pick_flag = True
                    break
            for putback_TP_start_pos in putback_TP_start_pos_list:
                if (putback_timeslot - relax_range <= putback_TP_start_pos <= putback_timeslot + relax_range) or (putback_timeslot - relax_range <= putback_TP_start_pos + window_size <= putback_timeslot + relax_range):    
                    putback_flag = True
                    break

            if pick_flag and putback_flag:
                result_dict['success'] += 1
    return result_dict

def get_adjacent_pick_putback_fp(X_test_dict, fp_result_dict, prediction_dict, window_size, confidnece):
    import numpy as np
    gesture_list = ['pick', 'putback']
    fp_count = 0

    for data_index in X_test_dict:
        all_start_pos_list = X_test_dict[data_index]['all_start_pos_list']

        # get all pick prediction pos
        pick_prediction_start_pos_list = []
        for pos_index, start_pos in enumerate(all_start_pos_list):
            end_pos = start_pos + window_size
            prediction = prediction_dict[data_index][pos_index]  
            max_index = np.argmax(prediction)
            if max_index == 0:  # 0: pick
                if prediction[max_index] > confidnece:
                    pick_prediction_start_pos_list.append(start_pos)

        putback_FP_start_pos_list = fp_result_dict[data_index]['putback']['start']

        for index, putback_FP_start in enumerate(putback_FP_start_pos_list):
            if index == 0:
                for pick_start in pick_prediction_start_pos_list:
                    if pick_start < putback_FP_start and putback_FP_start - pick_start < 332: # 332: median of adjacence_time_list
                        fp_count += 1
                        break
            elif index == len(putback_FP_start_pos_list) - 1:
                break

            if index + 1 <= len(putback_FP_start_pos_list) - 1:
                next_putback_FP_start = putback_FP_start_pos_list[index + 1]
                for pick_start in pick_prediction_start_pos_list:
                    if putback_FP_start < pick_start < next_putback_FP_start and next_putback_FP_start - pick_start < 332: # 332: median of adjacence_time_list
                        fp_count += 1
                        break
    return fp_count

def get_time_from_index(pos):
    minutes = int(pos / 50 / 60)  # sample rate = 50
    seconds = int(pos / 50 - minutes * 60)  # sample rate = 50
    return '%d:%d' % (minutes, seconds)