from typing import List

import antropy as ent
import numpy as np
import pandas as pd
from sklearn import svm
from tqdm import trange
import copy
from tool import *

TEMPLATE_NUM = 5

DATA_TYPE = ["acc", "q"]
GESTURE_LIST = ["pick", "putback"]
relax_range = 150

def read_acc_comparison_data(data_index, signal_data_path):
    signal = {'acc': {'x': [], 'y': [], 'z': [], 'mag': []}, 'q': {'x': [], 'y': [], 'z': [], 'mag': []}}
    motion_index_set = set()
    label_index_dict = {}
    for g in GESTURE_LIST:
        label_index_dict[g] = set()


    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)
        if data_type == 'm':
            motion_index = int(w)
            motion_index_set.add(motion_index)
        if data_type == 'l':
            label_data = w.split('-')
            # fix bugs
            # replace motion_index as time
            gesture, time = label_data[0], int(label_data[1])
            if gesture in GESTURE_LIST:
                label_gesture_dict[data_index][gesture].add(time)
                start_index = max(0, time)
                end_index = time + 1
                for t in range(start_index, end_index):
                    label_index_dict[gesture].add(t)

    return signal, label_index_dict, sorted(motion_index_set)


def compute_mcr(seq):
    mean = np.mean(seq)
    count = 0
    for i in range(1, len(seq)-1):
        if (seq[i] > mean and seq[i-1] <= mean) or (seq[i] <= mean and seq[i-1] > mean):
            count += 1
    return count


def compute_sub_windows_feature(x: List, y: List, z: List, window_size=20, step=10):
    """[summary]

    Args:
        x (List): [description]
        y (List): [description]
        z (List): [description]
        window_size (int, optional): [description]. Defaults to 20.
        step (int, optional): [description]. Defaults to 10.

    Returns:
        max mean:
        max rise
        max drop
    """
    assert len(x) == len(y) == len(z)

    def helper(seq):
        mean_list = []
        for start_pos in range(0, len(seq)-window_size, step):
            sub_seq = seq[start_pos:start_pos+window_size]
            mean_list.append(np.mean(sub_seq))

        diff_list = np.diff(mean_list)

        max_mean = np.max(mean_list)
        max_rise = np.max(diff_list)
        max_drop = np.min(diff_list)
        return [max_mean, max_rise, max_drop]

    ret = list(zip(helper(x), helper(y), helper(z)))
    return list(ret[0]), list(ret[1]), list(ret[2])


def compute_covariance(x, y, z):
    """compute xy, xz, yz covariance
    """
    cov_mat_xy = np.stack((x, y), axis=0)
    cov_mat_xz = np.stack((x, z), axis=0)
    cov_mat_yz = np.stack((y, z), axis=0)
    return [np.cov(cov_mat_xy)[0][1], np.cov(cov_mat_xz)[0][1], np.cov(cov_mat_yz)[0][1]]

def quaternion_rotation_matrix(Q):
    """
    Covert a quaternion into a full three-dimensional rotation matrix.
 
    Input
    :param Q: A 4 element array representing the quaternion (q0,q1,q2,q3) 
 
    Output
    :return: A 3x3 element matrix representing the full 3D rotation matrix. 
             This rotation matrix converts a point in the local reference 
             frame to a point in the global reference frame.
    """
    # Extract the values from Q
    q0 = Q[0]
    q1 = Q[1]
    q2 = Q[2]
    q3 = Q[3]
     
    # First row of the rotation matrix
    r00 = 2 * (q0 * q0 + q1 * q1) - 1
    r01 = 2 * (q1 * q2 - q0 * q3)
    r02 = 2 * (q1 * q3 + q0 * q2)
     
    # Second row of the rotation matrix
    r10 = 2 * (q1 * q2 + q0 * q3)
    r11 = 2 * (q0 * q0 + q2 * q2) - 1
    r12 = 2 * (q2 * q3 - q0 * q1)
     
    # Third row of the rotation matrix
    r20 = 2 * (q1 * q3 - q0 * q2)
    r21 = 2 * (q2 * q3 + q0 * q1)
    r22 = 2 * (q0 * q0 + q3 * q3) - 1
     
    # 3x3 rotation matrix
    rot_matrix = np.array([[r00, r01, r02],
                           [r10, r11, r12],
                           [r20, r21, r22]])
                            
    return rot_matrix

def get_linear_acc(start_pos, end_pos):
    linear_acc_x = []
    linear_acc_y = []
    linear_acc_z = []
    linear_acc_mag = []

    acc_x = list(np.array(signal['acc']['x'][start_pos:start_pos+window_size], dtype=np.float32))
    acc_y = list(np.array(signal['acc']['y'][start_pos:start_pos+window_size], dtype=np.float32))
    acc_z = list(np.array(signal['acc']['z'][start_pos:start_pos+window_size], dtype=np.float32))
    q_x = list(np.array(signal['q']['x'][start_pos:start_pos+window_size], dtype=np.float32))
    q_y = list(np.array(signal['q']['y'][start_pos:start_pos+window_size], dtype=np.float32))
    q_z = list(np.array(signal['q']['z'][start_pos:start_pos+window_size], dtype=np.float32))
    q_mag = list(np.array(signal['q']['mag'][start_pos:start_pos+window_size], dtype=np.float32))

    for i in range(end_pos - start_pos):
        acc_array = np.array([[acc_x[i]], [acc_y[i]], [acc_z[i]]])
        rot_matrix = quaternion_rotation_matrix([q_mag[i], q_x[i], q_y[i], q_z[i]])
        rot_acc_array = rot_matrix.dot(acc_array)
        rot_acc_array[2][0] -= 1  # z axis subtract 1g
        linear_acc_x.append(rot_acc_array[0][0])
        linear_acc_y.append(rot_acc_array[1][0])
        linear_acc_z.append(rot_acc_array[2][0])
        linear_acc_mag.append((rot_acc_array[0][0] ** 2 + rot_acc_array[1][0] ** 2 + rot_acc_array[2][0] ** 2) ** 0.5)
    return linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag

def preproc(signal, label_index_dict, window_size, step, motion_index_set):
    """Preprocess the data and generate the features.

    Args:
        signal ([type]): [description]
        label_index_dict ([type]): [description]
        window_size (int, optional): [description]. Defaults to 100.
        step (int, optional): [description]. Defaults to 50.

    Returns:
        features (nd.array) of shape (n_smaples, n_features)
        labels (nd.array) of shape (n_samples,)
    """
    seq_length_set = set([len(v) for v in signal['acc'].values()] + [len(v) for v in signal['q'].values()])
    assert len(seq_length_set) == 1

    seq_length = seq_length_set.pop()
    features_list = []
    label_list = []
    valid_gesture_time = {}
    for g in GESTURE_LIST:
        valid_gesture_time[g] = set()
    start_pos_list = []
    all_features_list = []
    all_label_list = []
    all_start_pos_list = []

    for start_pos in trange(0, seq_length-window_size, step):
        valid_window = False
        for motion_index in motion_index_set:
            if start_pos - relax_range < motion_index and motion_index < start_pos+window_size+relax_range:
                valid_window = True
                break
            # label 0: pick
            # label 1: putback
            # label 2: none

        if valid_window:
            label = -1
            if any(i in label_index_dict['pick'] for i in range(start_pos, start_pos+window_size)):
                label = 0

            if any(i in label_index_dict['putback'] for i in range(start_pos, start_pos+window_size)):
                label = 1
            # elif label == -1:
            #     label = 0

            acc_x = list(np.array(signal['acc']['x'][start_pos:start_pos+window_size], dtype=np.float32))
            acc_y = list(np.array(signal['acc']['y'][start_pos:start_pos+window_size], dtype=np.float32))
            acc_z = list(np.array(signal['acc']['z'][start_pos:start_pos+window_size], dtype=np.float32))
            acc_mag = list(np.array(signal['acc']['mag'][start_pos:start_pos+window_size], dtype=np.float32))
            acc_len = len(acc_x)
            acc_xy = []
            acc_xz = []
            acc_yz = []
            for i in range(acc_len):
                acc_xy.append(np.sqrt(acc_x[i] ** 2 + acc_y[i] ** 2))
                acc_xz.append(np.sqrt(acc_x[i] ** 2 + acc_z[i] ** 2))
                acc_yz.append(np.sqrt(acc_y[i] ** 2 + acc_z[i] ** 2))

            q_x = list(np.array(signal['q']['x'][start_pos:start_pos+window_size], dtype=np.float32))
            q_y = list(np.array(signal['q']['y'][start_pos:start_pos+window_size], dtype=np.float32))
            q_z = list(np.array(signal['q']['z'][start_pos:start_pos+window_size], dtype=np.float32))
            q_mag = list(np.array(signal['q']['mag'][start_pos:start_pos+window_size], dtype=np.float32))
            q_len = len(q_x)
            q_xy = []
            q_xz = []
            q_yz = []
            for i in range(q_len):
                q_xy.append(np.sqrt(q_x[i] ** 2 + q_y[i] ** 2))
                q_xz.append(np.sqrt(q_x[i] ** 2 + q_z[i] ** 2))
                q_yz.append(np.sqrt(q_y[i] ** 2 + q_z[i] ** 2))

            # by Direction Cosine Matrix IMU: Theory, page 9
            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag = get_linear_acc(start_pos, start_pos+window_size)
            linear_acc_len = len(linear_acc_x)
            linear_acc_xy = []
            linear_acc_xz = []
            linear_acc_yz = []
            for i in range(linear_acc_len):
                linear_acc_xy.append(np.sqrt(linear_acc_x[i] ** 2 + linear_acc_y[i] ** 2))
                linear_acc_xz.append(np.sqrt(linear_acc_x[i] ** 2 + linear_acc_z[i] ** 2))
                linear_acc_yz.append(np.sqrt(linear_acc_y[i] ** 2 + linear_acc_z[i] ** 2))

            mean = [np.mean(seq) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            std = [np.std(seq) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            _min = [np.min(seq) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            _max = [np.max(seq) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]   

            _firstq = [np.quantile(seq, 0.25) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            _secondq = [np.quantile(seq, 0.5) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            _thirdq = [np.quantile(seq, 0.75) for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                                            q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                                            linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]]

            bands = []
            for seq in [acc_x, acc_y, acc_z, acc_mag, acc_xy, acc_xz, acc_yz,
                        q_x, q_y, q_z, q_mag, q_xy, q_xz, q_yz,
                        linear_acc_x, linear_acc_y, linear_acc_z, linear_acc_mag, linear_acc_xy, linear_acc_xz, linear_acc_yz]:
                bands += list(np.abs(np.fft.fft(seq, n=25))[:10])

            feature = mean + std + _min + _max + _firstq + _secondq + _thirdq + bands
            
            if label > -1:
                features_list.append(feature) 
                label_list.append(label)
                start_pos_list.append(start_pos)
                
            all_features_list.append(feature) 
            all_label_list.append(label)
            all_start_pos_list.append(start_pos)


    return np.array(features_list, dtype=np.float32), label_list, start_pos_list, np.array(all_features_list, dtype=np.float32), all_label_list, all_start_pos_list

def build_model():
    clf = svm.SVC(kernel='linear', probability=True)
    return clf

if __name__ == '__main__':
    window_size = 50
    step = 25
    test_files = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']
    # test_files = ['SensorFusion_07_10_2021-16_20_38']
    test_file_num = len(test_files)
    training_files = ['training1', 'training2', 'training3']
    # training_files = ['training2']
    training_file_num = len(training_files)
    training_data_index_list = [i for i in range(test_file_num, test_file_num + training_file_num)]
    
    csv_filenames = test_files + training_files
    X_train = []
    y_train = []
    X_test_dict = {}
    label_gesture_dict = {}
    for index, csv_filename in enumerate(csv_filenames):
        label_gesture_dict[index] = {}
        for g in GESTURE_LIST:
            label_gesture_dict[index][g] = set()
        # label_data_path = "log/%s_label.csv" % (csv_filename)
        # read_acc_comparison_data(index, label_data_path)

        signal_data_path = "../real_data/%s.csv" % (csv_filename)
        signal, label_index_dict, motion_index_set = read_acc_comparison_data(index, signal_data_path)
        features_list, label_list, start_pos_list, all_features_list, all_label_list, all_start_pos_list = preproc(signal, label_index_dict, window_size, step, motion_index_set)
        if index in training_data_index_list:
            for feature_list in features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_train.append(feature_array)
            y_train += label_list
        else:
            X_test_dict[index] = { 'feature_array': [], 'all_start_pos_list': [] }
            for feature_list in all_features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_test_dict[index]['feature_array'].append(feature_array)
            X_test_dict[index]['all_start_pos_list'] += all_start_pos_list

    dict_npy_path = 'serend_%d_%d_X_test_dict.npy' % (window_size, step)
    X_train_array = np.array(X_train)
    y_train_array = np.array(y_train)
    np.save('serend_%d_%d_X_train_array.npy' % (window_size, step), X_train_array)
    np.save('serend_%d_%d_y_train_array.npy' % (window_size, step), y_train_array)
    np.save(dict_npy_path, X_test_dict)

    # X_train_array = np.load('serend_%d_%d_X_train_array.npy' % (window_size, step))
    # y_train_array = np.load('serend_%d_%d_y_train_array.npy' % (window_size, step))
    # X_npy = np.load(dict_npy_path, allow_pickle=True)
    # X_test_dict = X_npy.item()

    model = build_model()
    model.fit(X_train_array, y_train_array)
    prediction_dict = {}
    confidnece = 0.8
    for data_index in X_test_dict:
        X_test_array = np.array(X_test_dict[data_index]['feature_array'])
        prediction_dict[data_index] = []
        outputs = model.predict_proba(X_test_array)  
        for output in outputs:
            prediction_dict[data_index].append(output)

    tp_fn_result_dict = get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_fp_num_dict, FP_dict = get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    adjacent_result_dict = get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_adjacence_fp_num = get_adjacent_pick_putback_fp(X_test_dict, FP_dict, prediction_dict, window_size, confidnece)

    print("----All----")
    for g in GESTURE_LIST:
        print("gesture: ", g)
        recall = tp_fn_result_dict[g]['success'] / tp_fn_result_dict[g]['total']
        precision = tp_fn_result_dict[g]['success'] / (tp_fn_result_dict[g]['success'] + total_fp_num_dict[g])
        print("All recall: ")
        print(tp_fn_result_dict[g]['success'], tp_fn_result_dict[g]['total'], str(recall * 100) + '%')
        print("All precision: ")
        print(tp_fn_result_dict[g]['success'], total_fp_num_dict[g], str(precision * 100) + '%')
        print("All F1-score: ")
        print(2 * precision * recall / (precision + recall))
    adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
    adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + total_adjacence_fp_num)
    print("All adjacent recall: ")
    print(adjacent_result_dict['success'], adjacent_result_dict['total'], adjacence_recall)
    print("All adjacent precision: ")
    print(adjacent_result_dict['success'], total_adjacence_fp_num, adjacence_precision)
    print("All adjacent F1-score: ")
    print(2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))