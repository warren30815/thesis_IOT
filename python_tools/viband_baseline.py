from typing import List

import antropy as ent
import numpy as np
import pandas as pd
from sklearn import svm
from tqdm import trange
import copy
from tool import *

DATA_TYPE = ["acc", "q"]
GESTURE_LIST = ["pick", "putback"]
relax_range = 150

def read_acc_comparison_data(data_index, signal_data_path):
    signal = {'acc': {'x': [], 'y': [], 'z': [], 'mag': []}, 'q': {'x': [], 'y': [], 'z': [], 'mag': []}}
    motion_index_set = set()
    label_index_dict = {}
    for g in GESTURE_LIST:
        label_index_dict[g] = set()


    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)
        if data_type == 'm':
            motion_index = int(w)
            motion_index_set.add(motion_index)
        if data_type == 'l':
            label_data = w.split('-')
            # fix bugs
            # replace motion_index as time
            gesture, time = label_data[0], int(label_data[1])
            if gesture in GESTURE_LIST:
                label_gesture_dict[data_index][gesture].add(time)
                start_index = max(0, time)
                end_index = time + 1
                for t in range(start_index, end_index):
                    label_index_dict[gesture].add(t)

    return signal, label_index_dict, sorted(motion_index_set)


def compute_top_n(seq, n):
    sorted_index_array = np.argsort(seq)
    sorted_array = seq[sorted_index_array]
    result = list(sorted_array[-n : ])
    return result

def compute_1st_derivative(merge_spectra, last_spectra):
    result = []
    merge_spectra_len = len(merge_spectra)
    for i in range(merge_spectra_len):
        result.append(merge_spectra[i] - last_spectra[i])
    return result 

def compute_band_ratios(merge_spectra, last_spectra):
    result = []
    merge_spectra_len = len(merge_spectra)
    for i in range(merge_spectra_len):
        result.append(merge_spectra[i] / last_spectra[i])
    return result

def preproc(signal, label_index_dict, window_size, step, motion_index_set):
    """Preprocess the data and generate the features.

    Args:
        signal ([type]): [description]
        label_index_dict ([type]): [description]
        window_size (int, optional): [description]. Defaults to 100.
        step (int, optional): [description]. Defaults to 50.

    Returns:
        features (nd.array) of shape (n_smaples, n_features)
        labels (nd.array) of shape (n_samples,)
    """
    seq_length = len(signal['acc']['x'])
    features_list = []
    label_list = []
    valid_gesture_time = {}
    for g in GESTURE_LIST:
        valid_gesture_time[g] = set()
    start_pos_list = []
    all_features_list = []
    all_label_list = []
    all_start_pos_list = []

    last_spectra = None
    last_spectra_assigned = False
    for start_pos in trange(0, seq_length-window_size, step):
        acc_x = list(np.array(signal['acc']['x'][start_pos:start_pos+window_size], dtype=np.float32))
        acc_y = list(np.array(signal['acc']['y'][start_pos:start_pos+window_size], dtype=np.float32))
        acc_z = list(np.array(signal['acc']['z'][start_pos:start_pos+window_size], dtype=np.float32))
        power_spectrum_acc_x = np.abs(np.fft.fft(acc_x))[1:]
        power_spectrum_acc_y = np.abs(np.fft.fft(acc_y))[1:]
        power_spectrum_acc_z = np.abs(np.fft.fft(acc_z))[1:]
        fft_acc_len = len(power_spectrum_acc_x)
        merge_spectra = []
        for i in range(fft_acc_len):
            merge_spectra.append(max(power_spectrum_acc_x[i], power_spectrum_acc_y[i], power_spectrum_acc_z[i]))
        merge_spectra = np.array(merge_spectra, dtype=np.float32)
        if last_spectra_assigned:
            valid_window = False
            for motion_index in motion_index_set:
                if start_pos - relax_range < motion_index and motion_index < start_pos+window_size+relax_range:
                    valid_window = True
                    break
                # label 0: pick
                # label 1: putback
                # label 2: none

            if valid_window:
                label = -1
                if any(i in label_index_dict['pick'] for i in range(start_pos, start_pos+window_size)):
                    label = 0

                if any(i in label_index_dict['putback'] for i in range(start_pos, start_pos+window_size)):
                    label = 1
                # elif label == -1:
                #     label = 0

                mean = [np.mean(merge_spectra)]

                _sum = [np.sum(merge_spectra)]

                _min = [np.min(merge_spectra)]

                _max = [np.max(merge_spectra)]

                _1st_derivative = compute_1st_derivative(merge_spectra, last_spectra)

                _median = [np.median(merge_spectra)]

                std = [np.std(merge_spectra)]

                _range = [np.max(merge_spectra) - np.min(merge_spectra)]

                _band_ratios = compute_band_ratios(merge_spectra, last_spectra)

                top_n_peaks = compute_top_n(merge_spectra, 5)

                feature = mean + _sum + _min + _max + _1st_derivative + _median + std + _range + _band_ratios + top_n_peaks 
                # feature = mean + _sum + _min + _max + _1st_derivative + _median + std + _range + top_n_peaks 

                if label > -1:
                    features_list.append(feature) 
                    label_list.append(label)
                    start_pos_list.append(start_pos)
                
                all_features_list.append(feature) 
                all_label_list.append(label)
                all_start_pos_list.append(start_pos)

        last_spectra = merge_spectra
        last_spectra_assigned = True
    return np.array(features_list, dtype=np.float32), label_list, start_pos_list, np.array(all_features_list, dtype=np.float32), all_label_list, all_start_pos_list

def build_model():
    clf = svm.SVC(kernel='poly', probability=True)
    return clf

if __name__ == '__main__':
    window_size = 150
    step = 150
    test_files = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']
    # test_files = ['SensorFusion_07_10_2021-16_20_38']
    test_file_num = len(test_files)
    training_files = ['training1', 'training2', 'training3']
    # training_files = ['training2']
    training_file_num = len(training_files)
    training_data_index_list = [i for i in range(test_file_num, test_file_num + training_file_num)]
    
    csv_filenames = test_files + training_files
    X_train = []
    y_train = []
    X_test_dict = {}
    label_gesture_dict = {}
    for index, csv_filename in enumerate(csv_filenames):
        label_gesture_dict[index] = {}
        for g in GESTURE_LIST:
            label_gesture_dict[index][g] = set()
        # label_data_path = "log/%s_label.csv" % (csv_filename)
        # read_acc_comparison_data(index, label_data_path)

        signal_data_path = "../real_data/%s.csv" % (csv_filename)
        signal, label_index_dict, motion_index_set = read_acc_comparison_data(index, signal_data_path)
        features_list, label_list, start_pos_list, all_features_list, all_label_list, all_start_pos_list = preproc(signal, label_index_dict, window_size, step, motion_index_set)
        if index in training_data_index_list:
            for feature_list in features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_train.append(feature_array)
            y_train += label_list
        else:
            X_test_dict[index] = { 'feature_array': [], 'all_start_pos_list': [] }
            for feature_list in all_features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_test_dict[index]['feature_array'].append(feature_array)
            X_test_dict[index]['all_start_pos_list'] += all_start_pos_list

    X_train_array = np.array(X_train)
    y_train_array = np.array(y_train)
    np.save('viband_%d_%d_X_train_array.npy' % (window_size, step), X_train_array)
    np.save('viband_%d_%d_y_train_array.npy' % (window_size, step), y_train_array)
    dict_npy_path = 'viband_%d_%d_X_test_dict.npy' % (window_size, step)
    np.save(dict_npy_path, X_test_dict)

    # X_train_array = np.load('viband_%d_%d_X_train_array.npy' % (window_size, step))
    # y_train_array = np.load('viband_%d_%d_y_train_array.npy' % (window_size, step))
    # X_npy = np.load(dict_npy_path, allow_pickle=True)
    # X_test_dict = X_npy.item()

    model = build_model()
    model.fit(X_train_array, y_train_array)
    prediction_dict = {}
    confidnece = 0.55
    for data_index in X_test_dict:
        X_test_array = np.array(X_test_dict[data_index]['feature_array'])
        prediction_dict[data_index] = []
        outputs = model.predict_proba(X_test_array)  
        for output in outputs:
            prediction_dict[data_index].append(output)

    tp_fn_result_dict = get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_fp_num_dict, FP_dict = get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    adjacent_result_dict = get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_adjacence_fp_num = get_adjacent_pick_putback_fp(X_test_dict, FP_dict, prediction_dict, window_size, confidnece)

    print("----All----")
    for g in GESTURE_LIST:
        print("gesture: ", g)
        recall = tp_fn_result_dict[g]['success'] / tp_fn_result_dict[g]['total']
        precision = tp_fn_result_dict[g]['success'] / (tp_fn_result_dict[g]['success'] + total_fp_num_dict[g])
        print("All recall: ")
        print(tp_fn_result_dict[g]['success'], tp_fn_result_dict[g]['total'], str(recall * 100) + '%')
        print("All precision: ")
        print(tp_fn_result_dict[g]['success'], total_fp_num_dict[g], str(precision * 100) + '%')
        print("All F1-score: ")
        print(2 * precision * recall / (precision + recall))
    adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
    adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + total_adjacence_fp_num)
    print("All adjacent recall: ")
    print(adjacent_result_dict['success'], adjacent_result_dict['total'], adjacence_recall)
    print("All adjacent precision: ")
    print(adjacent_result_dict['success'], total_adjacence_fp_num, adjacence_precision)
    print("All adjacent F1-score: ")
    print(2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))