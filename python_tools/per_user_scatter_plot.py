import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

male_index = [0,1,2,3,5,9,11,13,14,16]
xs = [0.7405, 0.6278, 0.6782, 0.7832, 0.8663, 0.7691, 0.7273, 0.7712, 0.7561, 0.6975, 0.839, 0.5953, 0.6863, 0.6191, 0.7181, 0.671, 0.7625, 0.5381, 0.8029, 0.6762]
ys = [24, 24, 24, 24, 23, 54, 52, 24, 20, 24, 23, 24, 25, 26, 25, 23, 24, 25, 25, 54]
zs = [162, 187, 176, 185, 157, 162, 153, 160, 167, 171, 163, 172, 155, 171, 178, 153, 175, 158, 158, 158]
print("p value")
gender_list = [1,1,1,1,0,1,0,0,0,1,0,1,0,1,1,0,1,0,0,0]
print("F1 vs gender")
print(stats.ttest_ind(xs, gender_list)[1])
print("F1 vs age")
print(stats.ttest_ind(xs, ys)[1])
print("F1 vs height")
print(stats.ttest_ind(xs, zs)[1])
print("pearson")
print("F1 vs gender")
print(stats.pearsonr(xs, gender_list)[0])
print("F1 vs age")
print(stats.pearsonr(xs, ys)[0])
print("F1 vs height")
print(stats.pearsonr(xs, zs)[0])

xs_male = []
ys_male = []
zs_male = []
xs_female = []
ys_female = []
zs_female = []
for index, (x, y, z) in enumerate(list(zip(xs, ys, zs))):
    if index in male_index:
        xs_male.append(x)
        ys_male.append(y)
        zs_male.append(z)
    else:
        xs_female.append(x)
        ys_female.append(y)
        zs_female.append(z)

marker = 'o'
color = 'b'
label = 'Male'
ax.scatter(xs_male, ys_male, zs_male, marker=marker, color=color, label=label)
marker = '^'
color = 'r'
label = 'Female'
ax.scatter(xs_female, ys_female, zs_female, marker=marker, color=color, label=label)

ax.legend(loc='upper right', bbox_to_anchor=(0.83, 0.8), shadow=True) 
ax.set_xlabel('Avg F1-score')
ax.set_ylabel('Age')
ax.set_zlabel('Height')

plt.show()