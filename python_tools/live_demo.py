import random
from itertools import count
import time
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mpl_toolkits import mplot3d
import pandas as pd

def read_acc_comparison_data(signal_data_path):
    signal = { 'acc': { 'x': [], 'y': [], 'z': [], 'mag': [] }, 'q': { 'x': [], 'y': [], 'z': [], 'mag': [] } }
    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)

    return signal

def animate(i):
    local_time_index = next(index_counter) # counter or x variable -> index
    for local_index in range(10 * local_time_index, 10 * (local_time_index + 1)):
        index_values.append(local_index)

    x = x_signal[10 * local_time_index: 10 * (local_time_index+1)]
    y = y_signal[10 * local_time_index: 10 * (local_time_index+1)]
    z = z_signal[10 * local_time_index: 10 * (local_time_index+1)]

    for index in range(10):
        acc_x_values.append(x[index])
        acc_y_values.append(y[index])
        acc_z_values.append(z[index])

    if local_time_index > 20:
        '''
        This helps in keeping the graph fresh and refreshes values after every 40 timesteps
        '''
        del index_values[:10]
        del acc_x_values[:10]
        del acc_y_values[:10]
        del acc_z_values[:10]
        ax.cla() # clears the values of the graph

    for index, pick_start in enumerate(pick_start_list):
        pick_end = pick_end_list[index]
        lower = 10 * local_time_index
        if pick_end - forward <= lower <= pick_end + 125 - forward:
            plt.axvspan(pick_start - forward, pick_end - forward, ymin = 0, ymax = 1, color ='darkgreen')

    for index, putback_start in enumerate(putback_start_list):
        putback_end = putback_end_list[index]
        lower = 10 * local_time_index
        if putback_end - forward <= lower <= putback_end + 125 - forward:
            plt.axvspan(putback_start - forward, putback_end - forward, ymin = 0, ymax = 1, color ='royalblue')  

    ax.plot(index_values, acc_x_values, linestyle='--', color='chocolate')
    ax.plot(index_values, acc_y_values, linestyle='--', color='gold')
    ax.plot(index_values, acc_z_values, linestyle='--', color='tab:purple')
    
    ax.legend(["x","y","z"])
    ax.set_xlabel("index")
    # ax.set_ylabel("Values for Three different variable")
    # plt.title('Acc live plot')
    time.sleep(.2) # keep refresh rate of 0.2 seconds

csv_filename = "SensorFusion_07_14_2021-11_46_27"
signal = read_acc_comparison_data("../real_data/%s.csv" % (csv_filename))
index_values = []
acc_x_values = []
acc_y_values = []
acc_z_values = []
forward = 8000
# GRID_NUM = 2
x_signal = list(signal["acc"]['x'])[forward:]
y_signal = list(signal["acc"]['y'])[forward:]
z_signal = list(signal["acc"]['z'])[forward:]
index_counter = count()

pick_start_list = [10035, 11265, 14475, 19155, 19965, 22290, 22290, 25440, 26190, 27345]
pick_end_list = [10110, 11340, 14535, 19215, 20025, 22350, 22350, 25500, 26250, 27405]
putback_start_list = [10155, 14685, 20175, 25755]
putback_end_list = [10230, 14775, 20235, 25815]

fig, ax = plt.subplots(figsize=(15, 5))
# for i in range(GRID_NUM):
#     ax[i].axes.xaxis.set_visible(False)
    # ax[i].axes.yaxis.set_visible(False)
ax.axes.xaxis.set_visible(False)
# ax.axes.yaxis.set_visible(False)
ani = FuncAnimation(plt.gcf(), animate, 1000)
plt.tight_layout()
plt.show()