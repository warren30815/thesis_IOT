#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import os
import csv
from scipy.fftpack import dct
from tool import *

GESTURE_LIST = ["pick", "putback", "norotateputback", "norotateputback_v2", "pick_v2"]
PASS_GESTURE_LIST = []
LABEL_GESTURE_LIST = ["pick", "putback"]
sequence_length = 60
bary = 30
step = 5
result_file_step = 15
relax_range = 150
NEED_FREQ_DOMAIN_POSTPROCESSING = True

def read_label_data(signal_label_path):
    df = pd.read_csv(signal_label_path)
    last_motion_index = -1
    start_list = []
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]

        if data_type == 'l':
            data = w.split("-")
            gesture, time = data[0], int(data[1])
            if gesture != "other":
                label_gesture_dict[gesture].add(time)

def read_acc_comparison_data(signal_data_path):
    signal = { 'acc': { 'x': [], 'y': [], 'z': [], 'mag': [] }, 'q': { 'x': [], 'y': [], 'z': [], 'mag': [] } }
    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)

    return signal

def get_reduced_result_dict(result_data_path, signal):
    reduced_result_dict = {}
    for g in GESTURE_LIST:
        reduced_result_dict[g] = { 'start': [], 'end': [], 'count': [] }

    last_gesture = ''
    new_flag = False
    tmp_putback_dict = { "putback": [], "norotateputback": [], "norotateputback_v2": [] }

    gesture_start_set_dict = { 'pick': set(), 'putback': set() }
    with open(result_data_path) as f:
        for line in f.readlines():
            if line.startswith('key'):
                data = line.split(" ")
                data = data[1].split("-")
                gesture, start, end = data[0], int(data[1]), int(data[2])
                if gesture not in PASS_GESTURE_LIST:
                    if gesture == 'pick' or gesture == 'pick_v2':
                        clear_result = clear_duplicate_pick_by_freq_domain_analysis(signal, start, end)
                        if clear_result == False:
                            gesture_start_set_dict['pick'].add(start)
                    elif gesture == 'putback' or gesture == 'norotateputback' or gesture == 'norotateputback_v2':
                        tmp_putback_dict[gesture].append(start)  # 因為result log的順序不一定是同樣手勢就會全部擺在一起，所以putback的後處理要等pick都讀取完

    for gesture in tmp_putback_dict.keys():
        tmp_putback_list = tmp_putback_dict[gesture]
        for start in tmp_putback_list:
            end = start + sequence_length

            first_flag = True
            first_flag = False
            for pick_start in gesture_start_set_dict['pick']:
                if pick_start <= start <= pick_start + 1015:  # 1015: 95-th percentile of adjacence_time_list
                    first_flag = True
                    break

            second_flag = True
            for pick_start in gesture_start_set_dict['pick']:
                if pick_start <= end <= pick_start + sequence_length:  # 拿的前置動作很像放，故刪掉
                    second_flag = False
                    break

            clear_result = False
            clear_result = clear_duplicate_putback_by_freq_domain_analysis(signal, gesture, start, end)

            if (first_flag == True) and (second_flag == True) and (clear_result == False):
                gesture_start_set_dict['putback'].add(start)

    gesture_start_set_dict['pick'] = sorted(gesture_start_set_dict['pick'])
    gesture_start_set_dict['putback'] = sorted(gesture_start_set_dict['putback'])
    for gesture in ['pick', 'putback']:
        for start in gesture_start_set_dict[gesture]:
            end = start + sequence_length
            if len(reduced_result_dict[gesture]['start']) == 0:
                reduced_result_dict[gesture]['start'].append(start)
                reduced_result_dict[gesture]['end'].append(end)
                reduced_result_dict[gesture]['count'].append(1)
                last_gesture = gesture
                new_flag = True
            else:
                last_end = reduced_result_dict[gesture]['end'][-1]
                if start <= last_end:
                    reduced_result_dict[gesture]['end'][-1] = end
                    reduced_result_dict[gesture]['count'][-1] += 1
                    new_flag = False
                else:
                    reduced_result_dict[gesture]['start'].append(start)
                    reduced_result_dict[gesture]['end'].append(end)
                    reduced_result_dict[gesture]['count'].append(1)
                    new_flag = True
    return reduced_result_dict

def clear_duplicate_pick_by_freq_domain_analysis(signal, start_pos, end_pos):
    if not NEED_FREQ_DOMAIN_POSTPROCESSING:
        return False
    # return: True -> clear this window
    SAMPLE_RATE = sequence_length
    DURATION = 1
    N = SAMPLE_RATE * DURATION

    acc_value_part_x = np.array(signal['acc']['x'][start_pos: start_pos + 60])
    acc_value_part_y = np.array(signal['acc']['y'][start_pos: start_pos + 60])
    acc_value_part_z = np.array(signal['acc']['z'][start_pos: start_pos + 60])

    dct_x = dct(acc_value_part_x)
    dct_y = dct(acc_value_part_y)
    dct_z = dct(acc_value_part_z)

    xf = np.abs(dct_x[1:11])
    yf = np.abs(dct_y[1:11])
    zf = np.abs(dct_z[1:11])
    if xf.std() < 1 and yf.std() < 1 and zf.std() < 1:
        return True

    return False

def clear_duplicate_putback_by_freq_domain_analysis(signal, gesture, start_pos, end_pos):
    if not NEED_FREQ_DOMAIN_POSTPROCESSING:
        return False
    # return: True -> clear this window
    SAMPLE_RATE = sequence_length
    DURATION = 1
    N = SAMPLE_RATE * DURATION

    acc_value_part_x = np.array(signal['acc']['x'][start_pos: start_pos + 60])
    acc_value_part_y = np.array(signal['acc']['y'][start_pos: start_pos + 60])
    acc_value_part_z = np.array(signal['acc']['z'][start_pos: start_pos + 60])

    dct_x = dct(acc_value_part_x)
    dct_y = dct(acc_value_part_y)
    dct_z = dct(acc_value_part_z)

    if gesture == 'putback' or gesture == 'norotateputback':
        xf = np.abs(dct_x[1:11])
        yf = np.abs(dct_y[1:11])
        zf = np.abs(dct_z[1:11])
        if xf.std() < 1.75 and yf.std() < 1.75 and zf.std() < 1.75:
            return True

        xf = np.abs(dct_x[1:3])  # 1~2Hz
        yf = np.abs(dct_y[1:3])  # 1~2Hz
        zf = np.abs(dct_z[1:3])  # 1~2Hz
        # print(xf, yf, zf)
        if xf[1] - xf[0] > 7.25 or yf[1] - yf[0] > 7.25 or zf[1] - zf[0] > 7.25:
            return True

    elif gesture == 'norotateputback_v2':
        xf = np.abs(dct_x[1:11])
        yf = np.abs(dct_y[1:11])
        zf = np.abs(dct_z[1:11])
        if xf.std() < 2.5 and yf.std() < 2.5 and zf.std() < 2.5:
            return True

    return False

def get_false_positives_num(reduced_result_dict):
    FP_dict = {}
    for g in LABEL_GESTURE_LIST:
        FP_dict[g] = { 'start': set(), 'end': set() }
        print("gesture: ", g)
        all_start_pos_list = reduced_result_dict[g]['start']
        all_end_pos_list = reduced_result_dict[g]['end']
        count_list = reduced_result_dict[g]['count']
        for index, start_pos in enumerate(all_start_pos_list):
            end_pos = all_end_pos_list[index]
            if count_list[index] != -1:  # To be deleted
                timeslots = label_gesture_dict[g]
                FP_flag = True
                for timeslot in timeslots:
                    if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):  
                        FP_flag = False
                        break

                if FP_flag == True:
                    FP_dict[g]['start'].add(start_pos)
                    FP_dict[g]['end'].add(end_pos)
        # if g == 'putback':
        # print("****************", g, "FP ****************")
        start_list = sorted(FP_dict[g]['start'])
        end_list = sorted(FP_dict[g]['end'])
        FP_pair_list = list(zip(start_list, end_list))
        for FP_pair in FP_pair_list:
            start = FP_pair[0]
            end = FP_pair[1]
            # print(start, end)
            # print('start at: %s, end at: %s' % (get_time_from_index(start), get_time_from_index(end)))
    return FP_dict

def read_recognition_result_data(reduced_result_dict):
    for g in GESTURE_LIST:
        if g == 'norotateputback':
            g = 'putback'
        start_index_list = reduced_result_dict[g]['start']
        end_index_list = reduced_result_dict[g]['end']
        count_list = reduced_result_dict[g]['count']
        for i, start_index in enumerate(start_index_list):
            if count_list[i] != -1:
                end_index = end_index_list[i]
                valid_gesture_time[g]['start'].append(start_index)
                valid_gesture_time[g]['end'].append(end_index)


csv_filenames = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_46_17']
# csv_filenames = ['training1', 'training2', 'training3']
# csv_filenames = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04']

all_pick_total = 0
all_pick_success = 0
all_pick_fp = 0
all_putback_total = 0
all_putback_success = 0
all_putback_fp = 0
adjacent_result_dict = {'total': 0, 'success': 0, 'FP': 0, 'pickTP': [], 'putbackTP': []}

adjacence_time_list = []
for csv_filename in csv_filenames:
    label_gesture_dict = {}
    for g in LABEL_GESTURE_LIST:
        label_gesture_dict[g] = set()

    signal_data_path = "../real_data/%s.csv" % (csv_filename)
    signal_label_path = "log/%s_label.csv" % (csv_filename)
    # result_data_path = "log/seq=%d_step=%d_%s_result.txt" % (sequence_length, result_file_step, csv_filename)
    result_data_path = "log/seq=%d_step=%d_bary=%d_%s_result.txt" % (sequence_length, result_file_step, bary, csv_filename)

    valid_gesture_time = {}
    for g in GESTURE_LIST:
        valid_gesture_time[g] = { 'start': [], 'end': [] }

    # generate label.csv
    if not os.path.isfile(signal_label_path):
        with open(signal_label_path, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['type','w','x','y','z'])
            signal_data_path = "../real_data/%s.csv" % (csv_filename)
            df = pd.read_csv(signal_data_path)
            for index, row in df.iterrows():
                data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
                if data_type == 'l':
                    label = w
                    writer.writerow([data_type, label])

    read_label_data(signal_label_path)
    if NEED_FREQ_DOMAIN_POSTPROCESSING:
        signal = read_acc_comparison_data(signal_data_path)
    else:
        signal = {}
    reduced_result_dict = get_reduced_result_dict(result_data_path, signal)
    FP_dict = get_false_positives_num(reduced_result_dict)
    read_recognition_result_data(reduced_result_dict)

    pick_total = 0  # true positive + false negative
    pick_success = 0  # true positive
    pick_fp = len(FP_dict['pick']['start'])  # false positive
    putback_total = 0  # true positive + false negative
    putback_success = 0  # true positive
    putback_fp = len(FP_dict['putback']['start'])  # false positive

    all_pick_fp += pick_fp
    all_putback_fp += putback_fp

    # [Region] adjacence experiment
    adjacent_result_dict['pickTP'] = []
    adjacent_result_dict['putbackTP'] = []

    # calculate individual TP
    for key in LABEL_GESTURE_LIST:
        start_list = [] 
        end_list = [] 
        timeslots = label_gesture_dict[key]
        for timeslot in timeslots:
            if key == "pick":
                pick_total += 1
                all_pick_total += 1
                for pos_index, start_pos in enumerate(valid_gesture_time["pick"]["start"]):
                    end_pos = valid_gesture_time["pick"]["end"][pos_index]
                    if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):
                        pick_success += 1
                        all_pick_success += 1
                        adjacent_result_dict['pickTP'].append([start_pos, end_pos])
                        break
            elif key == "putback":
                putback_total += 1
                all_putback_total += 1
                for pos_index, start_pos in enumerate(valid_gesture_time["putback"]["start"]):
                    end_pos = valid_gesture_time["putback"]["end"][pos_index]
                    if (timeslot - relax_range <= start_pos <= timeslot + relax_range) or (timeslot - relax_range <= end_pos <= timeslot + relax_range):
                        putback_success += 1
                        all_putback_success += 1
                        adjacent_result_dict['putbackTP'].append([start_pos, end_pos])
                        break

    # calculate adjacence label
    adjacent_list = get_adjacent_pick_putback_label(label_gesture_dict)
    adjacent_result_dict['total'] += len(adjacent_list)
    
    # calculate adjacence TP
    for adjacent_pair in adjacent_list:
        pick_timeslot = adjacent_pair[0]
        putback_timeslot = adjacent_pair[1]
        adjacence_time_list.append(putback_timeslot - pick_timeslot)

        pick_TP_start_end_pos_pair_list = adjacent_result_dict['pickTP']
        putback_TP_start_end_pos_pair_list = adjacent_result_dict['putbackTP']
        pick_flag = False
        putback_flag = False
        for pick_TP_start_end_pos_pair in pick_TP_start_end_pos_pair_list:
            pick_TP_start_pos = pick_TP_start_end_pos_pair[0]
            pick_TP_end_pos = pick_TP_start_end_pos_pair[1]
            for pos in range(pick_TP_start_pos, pick_TP_end_pos + 5, 5):
                if pick_timeslot - relax_range <= pos <= pick_timeslot + relax_range:
                    pick_flag = True
                    break
        for putback_TP_start_end_pos_pair in putback_TP_start_end_pos_pair_list:
            putback_TP_start_pos = putback_TP_start_end_pos_pair[0]
            putback_TP_end_pos = putback_TP_start_end_pos_pair[1]
            for pos in range(putback_TP_start_pos, putback_TP_end_pos + 5, 5):
                if putback_timeslot - relax_range <= pos <= putback_timeslot + relax_range:
                    putback_flag = True
                    break

        if pick_flag and putback_flag:
            adjacent_result_dict['success'] += 1

    # calculate adjacence FP
    pick_prediction_start_pos_list = sorted(reduced_result_dict['pick']['start'])
    putback_FP_start_pos_list = sorted(FP_dict['putback']['start'])
    for index, putback_FP_start in enumerate(putback_FP_start_pos_list):
        if index == 0:
            for pick_start in pick_prediction_start_pos_list:
                if pick_start < putback_FP_start and putback_FP_start - pick_start < 332: # 332: median of adjacence_time_list
                    adjacent_result_dict['FP'] += 1
                    break
        elif index == len(putback_FP_start_pos_list) - 1:
            break

        if index + 1 <= len(putback_FP_start_pos_list) - 1:
            next_putback_FP_start = putback_FP_start_pos_list[index + 1]
            for pick_start in pick_prediction_start_pos_list:
                if putback_FP_start < pick_start < next_putback_FP_start and next_putback_FP_start - pick_start < 332: # 332: median of adjacence_time_list
                    adjacent_result_dict['FP'] += 1
                    break
    # print(adjacent_result_dict['FP'])
    # [EndRegion]

    print('csv_filename', csv_filename)
    print("Recall: ")
    pick_recall = pick_success / pick_total
    putback_recall = putback_success / putback_total
    print("pick", pick_success, pick_total, str(pick_recall * 100) + '%')
    print("putback", putback_success, putback_total, str(putback_recall * 100) + '%')
    print("Precision: ")
    pick_precision = pick_success / (pick_success + pick_fp)
    putback_precision = putback_success / (putback_success + putback_fp)
    print("pick", pick_success, pick_fp, str(pick_precision * 100) + '%')
    print("putback", putback_success, putback_fp, str(putback_precision * 100) + '%')
    pick_f1_score = "%.4f" % (2 * pick_precision * pick_recall / (pick_precision + pick_recall))
    putback_f1_score = "%.4f" % (2 * putback_precision * putback_recall / (putback_precision + putback_recall))
    print("pick: ", pick_f1_score)
    print("putback: ", putback_f1_score)
    print("avg F1", (float(pick_f1_score) + float(putback_f1_score)) / 2)

# print(sum(adjacence_time_list) / len(adjacence_time_list)) # average adjacence time
# print(max(adjacence_time_list))
# print(min(adjacence_time_list))
# print(np.quantile(adjacence_time_list, 0.95))

print("----All----")
print("All recall: ")
pick_recall = all_pick_success / all_pick_total
putback_recall = all_putback_success / all_putback_total
print("pick", all_pick_success, all_pick_total, str(pick_recall * 100) + '%')
print("putback", all_putback_success, all_putback_total, str(putback_recall * 100) + '%')
print("All precision: ")
pick_precision = all_pick_success / (all_pick_success + all_pick_fp)
putback_precision = all_putback_success / (all_putback_success + all_putback_fp)
print("pick", all_pick_success, all_pick_fp, str(pick_precision * 100) + '%')
print("putback", all_putback_success, all_putback_fp, str(putback_precision * 100) + '%')
print("F1-score: ")
pick_f1_score = "%.4f" % (2 * pick_precision * pick_recall / (pick_precision + pick_recall))
putback_f1_score = "%.4f" % (2 * putback_precision * putback_recall / (putback_precision + putback_recall))
print("pick: ", pick_f1_score)
print("putback: ", putback_f1_score)

adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + adjacent_result_dict['FP'])
adjacence_f1_score = "%.4f" % (2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))
print("All adjacent recall: ")
print(adjacent_result_dict['success'], adjacent_result_dict['total'], adjacence_recall)
print("All adjacent precision: ")
print(adjacent_result_dict['success'], adjacent_result_dict['FP'], adjacence_precision)
print("All adjacent F1-score: ")
print(adjacence_f1_score)