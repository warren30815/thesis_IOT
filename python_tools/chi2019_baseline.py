from typing import List

import antropy as ent
import numpy as np
import pandas as pd
from sklearn import svm
from tqdm import trange
import copy
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.mobilenet import MobileNet
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.layers import Conv2D, Flatten
from tensorflow.keras.layers import Flatten
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
from tool import *

TEMPLATE_NUM = 5

DATA_TYPE = ["acc", "q"]
GESTURE_LIST = ["pick", "putback"]
relax_range = 150

def read_acc_comparison_data(data_index, signal_data_path):
    signal = {'acc': {'x': [], 'y': [], 'z': [], 'mag': []}, 'q': {'x': [], 'y': [], 'z': [], 'mag': []}}
    motion_index_set = set()
    label_index_dict = {}
    for g in GESTURE_LIST:
        label_index_dict[g] = set()


    df = pd.read_csv(signal_data_path)
    for index, row in df.iterrows():
        data_type, w, x, y, z = df['type'][index], df['w'][index], df['x'][index], df['y'][index], df['z'][index]
        if data_type == 'a':
            if len(signal["q"]['x']) > len(signal["acc"]['x']):  # to make sure one q match one a
                signal["acc"]['x'].append(x)
                signal["acc"]['y'].append(y)
                signal["acc"]['z'].append(z)
                signal["acc"]['mag'].append(w)
        if data_type == 'q':
            signal["q"]['x'].append(x)
            signal["q"]['y'].append(y)
            signal["q"]['z'].append(z)
            signal["q"]['mag'].append(w)
        if data_type == 'm':
            motion_index = int(w)
            motion_index_set.add(motion_index)
        if data_type == 'l':
            label_data = w.split('-')
            gesture, time = label_data[0], int(label_data[1])
            if gesture in GESTURE_LIST:
                label_gesture_dict[data_index][gesture].add(time)
                label_index_dict[gesture].add(time)

    return signal, label_index_dict, sorted(motion_index_set)


def compute_top_n(seq, n):
    sorted_index_array = np.argsort(seq)
    sorted_array = seq[sorted_index_array]
    result = list(sorted_array[-n : ])
    return result

def compute_1st_derivative(merge_spectra, last_spectra):
    result = []
    merge_spectra_len = len(merge_spectra)
    for i in range(merge_spectra_len):
        result.append(merge_spectra[i] - last_spectra[i])
    return result 

def compute_band_ratios(merge_spectra, last_spectra):
    result = []
    merge_spectra_len = len(merge_spectra)
    for i in range(merge_spectra_len):
        result.append(merge_spectra[i] / last_spectra[i])
    return result

def preproc(signal, label_index_dict, window_size, step, motion_index_set):
    """Preprocess the data and generate the features.

    Args:
        signal ([type]): [description]
        label_index_dict ([type]): [description]
        window_size (int, optional): [description]. Defaults to 100.
        step (int, optional): [description]. Defaults to 50.

    Returns:
        features (nd.array) of shape (n_smaples, n_features)
        labels (nd.array) of shape (n_samples,)
    """
    seq_length = len(signal['acc']['x'])
    features_list = []
    label_list = []
    start_pos_list = []
    all_features_list = []
    all_label_list = []
    all_start_pos_list = []
    valid_gesture_time = {}
    for g in GESTURE_LIST:
        valid_gesture_time[g] = set()

    for start_pos in trange(0, seq_length-window_size, step):
        valid_window = False
        for motion_index in motion_index_set:
            if start_pos - relax_range < motion_index and motion_index < start_pos+window_size+relax_range:
                valid_window = True
                break
            # label 0: pick
            # label 1: putback
            # label 2: none

        if valid_window:
            label = -1
            if any(i in label_index_dict['pick'] for i in range(start_pos, start_pos+window_size)):
                label = 0

            if any(i in label_index_dict['putback'] for i in range(start_pos, start_pos+window_size)):
                label = 1
            # elif label == -1:
            #     label = 0

            acc_x = np.array(signal['acc']['x'][start_pos:start_pos+window_size], dtype=np.float32)
            acc_y = np.array(signal['acc']['y'][start_pos:start_pos+window_size], dtype=np.float32)
            acc_z = np.array(signal['acc']['z'][start_pos:start_pos+window_size], dtype=np.float32)

            frame_num = 3
            merge_spectra = np.zeros((3,256,48))
            for frame in range(frame_num):
                frame_acc_x = acc_x[50*frame: 50*(frame+1)]
                frame_acc_y = acc_y[50*frame: 50*(frame+1)]
                frame_acc_z = acc_z[50*frame: 50*(frame+1)]
                power_spectrum_acc_x = np.reshape(np.abs(np.fft.fft(frame_acc_x, n=4096)[:4096]), (256, 16))
                power_spectrum_acc_y = np.reshape(np.abs(np.fft.fft(frame_acc_y, n=4096)[:4096]), (256, 16))
                power_spectrum_acc_z = np.reshape(np.abs(np.fft.fft(frame_acc_z, n=4096)[:4096]), (256, 16))
                for i in range(256):
                    merge_spectra[0][i][16*frame: 16*(frame+1)] = power_spectrum_acc_x[i]
                    merge_spectra[1][i][16*frame: 16*(frame+1)] = power_spectrum_acc_y[i]
                    merge_spectra[2][i][16*frame: 16*(frame+1)] = power_spectrum_acc_z[i]
                
            merge_spectra = np.transpose(merge_spectra, (1, 2, 0))
            
            if label > -1:
                features_list.append(merge_spectra) 
                label_list.append(label)
                start_pos_list.append(start_pos)
                
            all_features_list.append(merge_spectra) 
            all_label_list.append(label)
            all_start_pos_list.append(start_pos)

    return np.array(features_list, dtype=np.float32), label_list, start_pos_list, np.array(all_features_list, dtype=np.float32), all_label_list, all_start_pos_list

# because our data is low-resolution in frequency domain, so we can't train whole VGG16 successfully (too deep). Therefore, we use only part of VGG16
def build_model():
    model = keras.Sequential(
        [
            keras.Input(shape=(256,48,3)),
            layers.Conv2D(64,kernel_size=(3,3),padding="same", activation="relu"),
            layers.BatchNormalization(),
            layers.Conv2D(64,kernel_size=(3,3),padding="same", activation="relu"),
            layers.BatchNormalization(),
            layers.MaxPooling2D(pool_size=(2, 2),strides=(2,2)),
            layers.Conv2D(128,kernel_size=(3,3),padding="same", activation="relu"),
            layers.BatchNormalization(),
            layers.Conv2D(128,kernel_size=(3,3),padding="same", activation="relu"),
            layers.BatchNormalization(),
            layers.MaxPooling2D(pool_size=(2, 2),strides=(2,2)),
            layers.Flatten(),
            # layers.Dense(2000, activation="relu"),
            layers.Dense(500, activation="relu"),
            layers.BatchNormalization(),
            layers.Dropout(0.4),
            layers.Dense(2, activation="softmax"),
        ]
    )
    # model = VGG16(include_top=False, weights='imagenet', input_shape=(256, 48, 3))
    # add new classifier layers
    # flat1 = Flatten()(model.layers[-1].output)
    # dense1 = Dense(2000, activation=tf.nn.relu)(flat1)
    # dense2 = Dense(500, activation=tf.nn.relu)(dense1)
    # dropout = Dropout(0.4)(flat1)
    # dense2 = flat1
    # output = Dense(2, activation='softmax')(dropout)
    # define new model
    # model = Model(inputs=model.inputs, outputs=output)
    # model.get_layer('block1_conv1').trainable = False
    # model.get_layer('block1_conv2').trainable = False
    # model.get_layer('block2_conv1').trainable = False
    # model.get_layer('block2_conv2').trainable = False
    # model.get_layer('block3_conv1').trainable = False
    # model.get_layer('block3_conv2').trainable = False
    # model.get_layer('block3_conv3').trainable = False
    # model.get_layer('block4_conv1').trainable = False
    # model.get_layer('block4_conv2').trainable = False
    # model.get_layer('block4_conv3').trainable = False
    # adam = Adam(lr=1e-3)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy']) 
    # # print(model.summary())
    return model

def one_hot_encoding(data):
    shape = (data.size, data.max()+1)
    one_hot = np.zeros(shape)
    rows = np.arange(data.size)
    one_hot[rows, data] = 1
    return one_hot

if __name__ == '__main__':
    window_size = 150
    step = 150
    test_files = ['SensorFusion_07_10_2021-16_20_38', 'SensorFusion_07_10_2021-16_33_04', 'SensorFusion_07_10_2021-16_46_11', 'SensorFusion_07_10_2021-17_00_10',
                 'SensorFusion_07_10_2021-17_16_07', 'SensorFusion_07_14_2021-17_45_14', 'SensorFusion_07_14_2021-17_55_13',
                'SensorFusion_07_14_2021-18_05_25', 'SensorFusion_07_14_2021-18_20_44', 'SensorFusion_07_14_2021-18_30_03',
                'SensorFusion_07_14_2021-15_17_13', 'SensorFusion_07_14_2021-15_29_52', 'SensorFusion_07_14_2021-15_45_19',
                'SensorFusion_07_14_2021-16_00_26', 'SensorFusion_07_14_2021-11_17_14', 'SensorFusion_07_14_2021-11_33_32',
                'SensorFusion_07_14_2021-11_46_27', 'SensorFusion_07_15_2021-15_15_30', 'SensorFusion_07_15_2021-15_30_25', 'SensorFusion_07_15_2021-15_46_17']
    # test_files = ['SensorFusion_07_10_2021-16_20_38']
    test_file_num = len(test_files)
    training_files = ['training1', 'training2', 'training3']
    # training_files = ['training2']
    training_file_num = len(training_files)
    training_data_index_list = [i for i in range(test_file_num, test_file_num + training_file_num)]

    csv_filenames = test_files + training_files
    X_train = []
    y_train = []
    X_test_dict = {}
    label_gesture_dict = {}

    total_feature_extraction_time_dict = {'train': [], 'test': []}
    avg_training_time_list = []
    avg_inference_time_list = []
    import time

    for index, csv_filename in enumerate(csv_filenames):
        label_gesture_dict[index] = {}
        for g in GESTURE_LIST:
            label_gesture_dict[index][g] = set()
        # label_data_path = "log/%s_label.csv" % (csv_filename)
        # read_acc_comparison_data(index, label_data_path)

        signal_data_path = "../real_data/%s.csv" % (csv_filename)
        signal, label_index_dict, motion_index_set = read_acc_comparison_data(index, signal_data_path)
        start_feature_extraction_time = time.time()
        features_list, label_list, start_pos_list, all_features_list, all_label_list, all_start_pos_list = preproc(signal, label_index_dict, window_size, step, motion_index_set)
        if index in training_data_index_list:
            for feature_list in features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_train.append(feature_array)
            y_train += label_list
            total_feature_extraction_time_dict['train'].append(time.time() - start_feature_extraction_time)
        else:
            X_test_dict[index] = { 'feature_array': [], 'all_start_pos_list': [] }
            for feature_list in all_features_list:
                feature_array = np.array(feature_list)
                feature_array = np.nan_to_num(feature_array)
                X_test_dict[index]['feature_array'].append(feature_array)
            X_test_dict[index]['all_start_pos_list'] += all_start_pos_list
            total_feature_extraction_time_dict['test'].append((time.time() - start_feature_extraction_time) / len(all_features_list))

    X_train_array = np.array(X_train)
    y_train_array = np.array(y_train)
    np.save('chi2019_%d_%d_X_train_array.npy' % (window_size, step), X_train_array)
    np.save('chi2019_%d_%d_y_train_array.npy' % (window_size, step), y_train_array)
    dict_npy_path = 'chi2019_%d_%d_X_test_dict.npy' % (window_size, step)
    np.save(dict_npy_path, X_test_dict)

    # X_train_array = np.load('chi2019_%d_%d_X_train_array.npy' % (window_size, step))
    # y_train_array = np.load('chi2019_%d_%d_y_train_array.npy' % (window_size, step))
    # X_npy = np.load(dict_npy_path, allow_pickle=True)
    # X_test_dict = X_npy.item()

    start_training_time = time.time()
    model = build_model()
    model.fit(x=X_train_array, y=one_hot_encoding(y_train_array), epochs=200, batch_size=4, verbose=2)
    avg_training_time_list.append(time.time() - start_training_time)
    prediction_dict = {}
    confidnece = 0.9
    relax_range = 150
    for data_index in X_test_dict:
        start_inference_time = time.time()
        X_test_array = np.array(X_test_dict[data_index]['feature_array'])
        prediction_dict[data_index] = []
        outputs = model.predict(X_test_array)  
        for output in outputs:
            prediction_dict[data_index].append(output)
        avg_inference_time_list.append((time.time() - start_inference_time) / len(outputs))

    tp_fn_result_dict = get_tp_and_fn_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_fp_num_dict, FP_dict = get_fp_num(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    adjacent_result_dict = get_adjacent_pick_putback_recall(X_test_dict, label_gesture_dict, prediction_dict, window_size, confidnece, relax_range)
    total_adjacence_fp_num = get_adjacent_pick_putback_fp(X_test_dict, FP_dict, prediction_dict, window_size, confidnece)

    print("----All----")
    for g in GESTURE_LIST:
        print("gesture: ", g)
        recall = tp_fn_result_dict[g]['success'] / tp_fn_result_dict[g]['total']
        precision = tp_fn_result_dict[g]['success'] / (tp_fn_result_dict[g]['success'] + total_fp_num_dict[g])
        print("All recall: ")
        print(tp_fn_result_dict[g]['success'], tp_fn_result_dict[g]['total'], str(recall * 100) + '%')
        print("All precision: ")
        print(tp_fn_result_dict[g]['success'], total_fp_num_dict[g], str(precision * 100) + '%')
        print("All F1-score: ")
        print(2 * precision * recall / (precision + recall))
    adjacence_recall = adjacent_result_dict['success'] / adjacent_result_dict['total']
    adjacence_precision = adjacent_result_dict['success'] / (adjacent_result_dict['success'] + total_adjacence_fp_num)
    print("All adjacent recall: ")
    print(adjacent_result_dict['success'], adjacent_result_dict['total'], adjacence_recall)
    print("All adjacent precision: ")
    print(adjacent_result_dict['success'], total_adjacence_fp_num, adjacence_precision)
    print("All adjacent F1-score: ")
    print(2 * adjacence_precision * adjacence_recall / (adjacence_precision + adjacence_recall))
    print("total train feature_extraction_time", np.array(total_feature_extraction_time_dict['train']).sum())
    print("total test feature_extraction_time", np.array(total_feature_extraction_time_dict['test']).mean())
    print("avg_training_time", np.array(avg_training_time_list).mean())
    print("avg_inference_time", np.array(avg_inference_time_list).mean())