會用到的資料夾，其他沒寫到的就不用動

real_data/: 20筆受試者test data與3筆training data，詳見資料夾內的README.txt

ExampleApp/: iOS app主要的code，詳見資料夾內的README.md

python_tools/: 流程圖內sliding window, 手勢辨識, 後處理與baseline、計算f1、畫圖的code，詳見資料夾內readme.txt

template_data/: 模板訊號資料，如名稱包含pick代表是拿取，包含putback代表是放回，共有五個系列，共17筆訊號，如下：
    (i) norotateputback
    (ii) norotateputback_v2
    (iii) putback
    (iv) pick
    (v) pick_v2
